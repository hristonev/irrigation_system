# Irrigation system

## Installation
```bash
sudo ln -s /opt/irrigation_system/irrigation.service /etc/systemd/system/
sudo systemctl enable irrigation
sudo systemctl daemon-reload
```