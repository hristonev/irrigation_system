import platform
import os
import signal

# Fake Pi
if platform.machine()[:3] != 'arm':
    import sys
    import fake_rpi

    sys.modules['RPi'] = fake_rpi.RPi  # Fake RPi
    sys.modules['RPi.GPIO'] = fake_rpi.RPi.GPIO  # Fake GPIO
    sys.modules['smbus'] = fake_rpi.smbus  # Fake smbus (I2C)
import sys
import yaml
import logging
from logging.handlers import TimedRotatingFileHandler
from queue import Queue
from src.dispatcher import Dispatcher
from datetime import datetime
from pytz import timezone
import time

os.chdir(os.path.dirname(os.path.realpath(sys.argv[0])))

# Load system config
with open('config.yaml', 'rt') as fp:
    config = yaml.safe_load(fp)


# Logger config
def timetz(*args):
    return datetime.now(tz).timetuple()


run = True
tz = timezone(config['general']['timezone'])
logging.basicConfig(level=0)
logging.Formatter.converter = timetz
handler = TimedRotatingFileHandler(filename=config['logger']['name'], when='D')
formatter = logging.Formatter(config['logger']['format'])
handler.setFormatter(formatter)
logger = logging.getLogger()
# Remove default handlers
for h in logger.handlers:
    logger.removeHandler(h)
logger.addHandler(handler)


def handler_stop_signals(signum, frame):
    global run
    run = False


if __name__ == '__main__':
    signal.signal(signal.SIGINT, handler_stop_signals)
    signal.signal(signal.SIGTERM, handler_stop_signals)
    q = Queue()
    t = [
        Dispatcher(config, q)
    ]
    try:
        for i in t:
            i.start()
    except:
        print('Dispatcher exception failed!')
        pass

    while run:
        time.sleep(1)
        pass

    q.put(True)

    for i in t:
        i.join()
