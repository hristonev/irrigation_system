<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.6.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="T2535-800G-TR">
<packages>
<package name="D2PACK">
<description>&lt;b&gt;D2PACK&lt;/b&gt;&lt;p&gt;
INTERNATIONAL RECTIFIER, irg4bc15ud-s.pdf</description>
<wire x1="-5.1308" y1="-4.0894" x2="5.1308" y2="-4.0894" width="0.254" layer="21"/>
<wire x1="5.1308" y1="-4.0894" x2="5.1308" y2="4.445" width="0.254" layer="51"/>
<wire x1="5.1308" y1="4.445" x2="3.1242" y2="5.8166" width="0.254" layer="51"/>
<wire x1="3.1242" y1="5.8166" x2="-3.3782" y2="5.8166" width="0.254" layer="51"/>
<wire x1="-3.3782" y1="5.8166" x2="-5.1308" y2="4.699" width="0.254" layer="51"/>
<wire x1="-5.1308" y1="4.699" x2="-5.1308" y2="4.445" width="0.254" layer="51"/>
<wire x1="-5.1308" y1="4.445" x2="-5.1308" y2="-4.0894" width="0.254" layer="51"/>
<wire x1="-5.1308" y1="4.445" x2="5.1308" y2="4.445" width="0.254" layer="51"/>
<wire x1="-5.1308" y1="-4.0894" x2="-5.1308" y2="-2.3114" width="0.254" layer="21"/>
<wire x1="5.1308" y1="-4.0894" x2="5.1308" y2="-2.3114" width="0.254" layer="21"/>
<text x="-5.588" y="7.239" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.81" y="-3.429" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.0988" y1="-9.525" x2="-1.9812" y2="-8.1026" layer="51"/>
<rectangle x1="-3.2512" y1="-8.1534" x2="-1.8288" y2="-6.731" layer="51"/>
<rectangle x1="-3.2512" y1="-6.731" x2="-1.8288" y2="-4.2418" layer="21"/>
<rectangle x1="1.9812" y1="-9.525" x2="3.0988" y2="-8.1026" layer="51"/>
<rectangle x1="1.8288" y1="-8.1534" x2="3.2512" y2="-6.731" layer="51"/>
<rectangle x1="1.8288" y1="-6.731" x2="3.2512" y2="-4.2418" layer="21"/>
<rectangle x1="-0.7112" y1="-6.731" x2="0.7112" y2="-4.2418" layer="21"/>
<smd name="1" x="-2.54" y="-8.89" dx="2.0828" dy="3.81" layer="1"/>
<smd name="3" x="2.54" y="-8.89" dx="2.0828" dy="3.81" layer="1"/>
<smd name="2" x="0" y="2.54" dx="11.43" dy="8.89" layer="1"/>
</package>
</packages>
<symbols>
<symbol name="TRIAC">
<wire x1="-2.54" y1="2.794" x2="-1.016" y2="0.508" width="0.254" layer="94"/>
<wire x1="-1.016" y1="0.508" x2="0.254" y2="2.794" width="0.254" layer="94"/>
<wire x1="0.254" y1="2.794" x2="-2.54" y2="2.794" width="0.254" layer="94"/>
<wire x1="1.016" y1="2.794" x2="0.254" y2="2.794" width="0.254" layer="94"/>
<wire x1="-0.254" y1="0.508" x2="1.016" y2="2.794" width="0.254" layer="94"/>
<wire x1="1.016" y1="2.794" x2="2.54" y2="0.508" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.508" x2="0" y2="0.508" width="0.254" layer="94"/>
<wire x1="0" y1="0.508" x2="-0.254" y2="0.508" width="0.254" layer="94"/>
<wire x1="1.016" y1="2.794" x2="2.54" y2="2.794" width="0.254" layer="94"/>
<wire x1="-1.905" y1="0.508" x2="-2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="0.508" x2="-1.905" y2="0.508" width="0.254" layer="94"/>
<wire x1="-1.905" y1="0.508" x2="-2.54" y2="0.508" width="0.254" layer="94"/>
<wire x1="-1.016" y1="0.508" x2="-0.254" y2="0.508" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="3.81" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="3.81" y="0" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A2" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="A1" x="0" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="G" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="T2535-800G-TR" prefix="U">
<description>25 A standard and Snubberless™ triacs - ST  &lt;a href="https://pricing.snapeda.com/parts/T2535-800G-TR/STMicroelectronics/view-part?ref=eda"&gt;Check availability&lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="TRIAC" x="5.08" y="0"/>
</gates>
<devices>
<device name="" package="D2PACK">
<connects>
<connect gate="G$1" pin="A1" pad="1"/>
<connect gate="G$1" pin="A2" pad="2"/>
<connect gate="G$1" pin="G" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="In Stock"/>
<attribute name="DESCRIPTION" value=" TRIAC Alternistor - Snubberless 800 V 25 A Surface Mount D2PAK "/>
<attribute name="MF" value="STMicroelectronics"/>
<attribute name="MP" value="T2535-800G-TR"/>
<attribute name="PACKAGE" value="D2PAK STMicroelectronics"/>
<attribute name="PRICE" value="None"/>
<attribute name="PURCHASE-URL" value="https://pricing.snapeda.com/search/part/T2535-800G-TR/?ref=eda"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-wago-500" urn="urn:adsk.eagle:library:195">
<description>&lt;b&gt;Wago Screw Clamps&lt;/b&gt;&lt;p&gt;
Grid 5.00 mm&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="W237-102" urn="urn:adsk.eagle:footprint:10676/1" library_version="2">
<description>&lt;b&gt;WAGO SCREW CLAMP&lt;/b&gt;</description>
<wire x1="-3.491" y1="-2.286" x2="-1.484" y2="-0.279" width="0.254" layer="51"/>
<wire x1="1.488" y1="-2.261" x2="3.469" y2="-0.254" width="0.254" layer="51"/>
<wire x1="-4.989" y1="-5.461" x2="4.993" y2="-5.461" width="0.1524" layer="21"/>
<wire x1="4.993" y1="3.734" x2="4.993" y2="3.531" width="0.1524" layer="21"/>
<wire x1="4.993" y1="3.734" x2="-4.989" y2="3.734" width="0.1524" layer="21"/>
<wire x1="-4.989" y1="-5.461" x2="-4.989" y2="-3.073" width="0.1524" layer="21"/>
<wire x1="-4.989" y1="-3.073" x2="-3.389" y2="-3.073" width="0.1524" layer="21"/>
<wire x1="-3.389" y1="-3.073" x2="-1.611" y2="-3.073" width="0.1524" layer="51"/>
<wire x1="-1.611" y1="-3.073" x2="1.615" y2="-3.073" width="0.1524" layer="21"/>
<wire x1="3.393" y1="-3.073" x2="4.993" y2="-3.073" width="0.1524" layer="21"/>
<wire x1="-4.989" y1="-3.073" x2="-4.989" y2="3.531" width="0.1524" layer="21"/>
<wire x1="4.993" y1="-3.073" x2="4.993" y2="-5.461" width="0.1524" layer="21"/>
<wire x1="-4.989" y1="3.531" x2="4.993" y2="3.531" width="0.1524" layer="21"/>
<wire x1="-4.989" y1="3.531" x2="-4.989" y2="3.734" width="0.1524" layer="21"/>
<wire x1="4.993" y1="3.531" x2="4.993" y2="-3.073" width="0.1524" layer="21"/>
<wire x1="1.615" y1="-3.073" x2="3.393" y2="-3.073" width="0.1524" layer="51"/>
<circle x="-2.5" y="-1.27" radius="1.4986" width="0.1524" layer="51"/>
<circle x="-2.5" y="2.2098" radius="0.508" width="0.1524" layer="21"/>
<circle x="2.5038" y="-1.27" radius="1.4986" width="0.1524" layer="51"/>
<circle x="2.5038" y="2.2098" radius="0.508" width="0.1524" layer="21"/>
<pad name="1" x="-2.5" y="-1.27" drill="1.1938" shape="long" rot="R90"/>
<pad name="2" x="2.5" y="-1.27" drill="1.1938" shape="long" rot="R90"/>
<text x="-5.04" y="-7.62" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-3.8462" y="-5.0038" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.532" y="0.635" size="1.27" layer="21" ratio="10">1</text>
<text x="0.421" y="0.635" size="1.27" layer="21" ratio="10">2</text>
</package>
</packages>
<packages3d>
<package3d name="W237-102" urn="urn:adsk.eagle:package:10688/1" type="box" library_version="2">
<description>WAGO SCREW CLAMP</description>
<packageinstances>
<packageinstance name="W237-102"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="KL" urn="urn:adsk.eagle:symbol:10675/1" library_version="2">
<circle x="1.27" y="0" radius="1.27" width="0.254" layer="94"/>
<text x="0" y="0.889" size="1.778" layer="95" rot="R180">&gt;NAME</text>
<pin name="KL" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="KL+V" urn="urn:adsk.eagle:symbol:10673/1" library_version="2">
<circle x="1.27" y="0" radius="1.27" width="0.254" layer="94"/>
<text x="-2.54" y="-3.683" size="1.778" layer="96">&gt;VALUE</text>
<text x="0" y="0.889" size="1.778" layer="95" rot="R180">&gt;NAME</text>
<pin name="KL" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="W237-102" urn="urn:adsk.eagle:component:10702/2" prefix="X" uservalue="yes" library_version="2">
<description>&lt;b&gt;WAGO SCREW CLAMP&lt;/b&gt;</description>
<gates>
<gate name="-1" symbol="KL" x="0" y="5.08" addlevel="always"/>
<gate name="-2" symbol="KL+V" x="0" y="0" addlevel="always"/>
</gates>
<devices>
<device name="" package="W237-102">
<connects>
<connect gate="-1" pin="KL" pad="1"/>
<connect gate="-2" pin="KL" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:10688/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="237-102" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="70K9898" constant="no"/>
<attribute name="POPULARITY" value="32" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="MOC3063M_">
<packages>
<package name="DIP8SMD">
<wire x1="-3.81" y1="3.175" x2="3.81" y2="3.175" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.175" x2="3.81" y2="-2.032" width="0.127" layer="21"/>
<wire x1="3.81" y1="-2.032" x2="3.81" y2="-3.175" width="0.127" layer="21"/>
<wire x1="3.81" y1="-3.175" x2="-3.81" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-3.175" x2="-3.81" y2="-2.032" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-2.032" x2="-3.81" y2="3.175" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-2.032" x2="3.81" y2="-2.032" width="0.127" layer="21"/>
<circle x="-2.794" y="-0.889" radius="0.381" width="0.127" layer="21"/>
<text x="-3.56558125" y="1.5281" size="1.018740625" layer="25">&gt;NAME</text>
<text x="-3.562890625" y="0" size="1.01796875" layer="27">&gt;VALUE</text>
<smd name="1" x="-2.54" y="-4.572" dx="1.524" dy="1.27" layer="1"/>
<smd name="2" x="0" y="-4.572" dx="1.524" dy="1.27" layer="1"/>
<smd name="3" x="2.54" y="-4.572" dx="1.524" dy="1.27" layer="1"/>
<smd name="4" x="2.54" y="4.572" dx="1.524" dy="1.27" layer="1"/>
<smd name="5" x="0" y="4.572" dx="1.524" dy="1.27" layer="1"/>
<smd name="6" x="-2.54" y="4.572" dx="1.524" dy="1.27" layer="1"/>
</package>
</packages>
<symbols>
<symbol name="OPTOTRIAC_ZC">
<wire x1="-7.62" y1="5.08" x2="7.62" y2="5.08" width="0.4064" layer="94"/>
<wire x1="7.62" y1="5.08" x2="7.62" y2="2.54" width="0.4064" layer="94"/>
<wire x1="7.62" y1="2.54" x2="7.62" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="7.62" y1="-2.54" x2="7.62" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="-7.62" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="-7.62" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="-2.54" x2="-7.62" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="2.54" x2="-7.62" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="-1.016" x2="-5.08" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-1.016" x2="-3.81" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-1.016" x2="-5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-2.54" x2="-5.08" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-1.016" x2="-5.08" y2="1.016" width="0.254" layer="94"/>
<wire x1="1.778" y1="-1.016" x2="3.048" y2="-1.016" width="0.254" layer="94"/>
<wire x1="3.048" y1="-1.016" x2="4.318" y2="-1.016" width="0.254" layer="94"/>
<wire x1="3.048" y1="-1.016" x2="3.048" y2="1.016" width="0.254" layer="94"/>
<wire x1="6.858" y1="1.016" x2="5.588" y2="1.016" width="0.254" layer="94"/>
<wire x1="5.588" y1="1.016" x2="4.318" y2="1.016" width="0.254" layer="94"/>
<wire x1="5.588" y1="1.016" x2="5.588" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-7.62" y1="2.54" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="1.016" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-2.54" x2="-7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="2.54" x2="4.318" y2="2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="-2.54" x2="4.318" y2="-2.54" width="0.254" layer="94"/>
<wire x1="4.318" y1="-2.54" x2="4.318" y2="-1.524" width="0.254" layer="94"/>
<wire x1="4.318" y1="1.016" x2="4.318" y2="2.54" width="0.254" layer="94"/>
<wire x1="4.318" y1="-1.524" x2="4.318" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-2.032" y1="-1.016" x2="-2.032" y2="-3.302" width="0.254" layer="94"/>
<wire x1="-2.032" y1="-3.302" x2="1.016" y2="-3.302" width="0.254" layer="94"/>
<wire x1="1.016" y1="-3.302" x2="1.016" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.016" y1="-2.54" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="1.016" y1="-1.778" x2="1.016" y2="-1.016" width="0.254" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="-0.508" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-0.508" y1="-1.016" x2="-2.032" y2="-1.016" width="0.254" layer="94"/>
<wire x1="4.318" y1="2.54" x2="-0.508" y2="2.54" width="0.254" layer="94"/>
<wire x1="-0.508" y1="2.54" x2="-0.508" y2="0" width="0.254" layer="94"/>
<wire x1="-0.508" y1="0" x2="-0.508" y2="-1.016" width="0.254" layer="94"/>
<wire x1="4.318" y1="-2.54" x2="1.016" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-3.302" y1="0.762" x2="-1.524" y2="2.032" width="0.254" layer="94"/>
<wire x1="-1.524" y1="2.032" x2="-2.286" y2="2.032" width="0.254" layer="94"/>
<wire x1="-1.524" y1="2.032" x2="-1.778" y2="1.27" width="0.254" layer="94"/>
<wire x1="-3.048" y1="-0.508" x2="-1.27" y2="0.762" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0.762" x2="-2.032" y2="0.762" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0.762" x2="-1.524" y2="0" width="0.254" layer="94"/>
<wire x1="3.048" y1="-1.016" x2="2.54" y2="-1.778" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.778" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<text x="-7.641609375" y="-7.641609375" size="1.783040625" layer="96">&gt;VALUE</text>
<text x="-7.6432" y="5.85978125" size="1.783409375" layer="95">&gt;NAME</text>
<text x="-1.526459375" y="-2.7985" size="1.272040625" layer="94" ratio="15">ZC</text>
<polygon width="0.254" layer="94">
<vertex x="-6.35" y="1.016"/>
<vertex x="-5.08" y="-1.016"/>
<vertex x="-3.81" y="1.016"/>
</polygon>
<polygon width="0.254" layer="94">
<vertex x="1.778" y="1.016"/>
<vertex x="3.048" y="-1.016"/>
<vertex x="4.318" y="1.016"/>
</polygon>
<polygon width="0.254" layer="94">
<vertex x="6.858" y="-1.016"/>
<vertex x="5.588" y="1.016"/>
<vertex x="4.318" y="-1.016"/>
</polygon>
<pin name="A" x="-12.7" y="2.54" visible="pad" length="middle"/>
<pin name="K" x="-12.7" y="-2.54" visible="pad" length="middle"/>
<pin name="T2" x="12.7" y="-2.54" visible="pad" length="middle" rot="R180"/>
<pin name="T1" x="12.7" y="2.54" visible="pad" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MOC3063M*" prefix="U">
<description>&lt;b&gt;Optotriac in DIP and SMD&lt;/b&gt;
&lt;p&gt;
LED:&lt;br&gt;
Vf: 1.2V&lt;br&gt;
If: 5 mA&lt;br&gt;
&lt;p&gt;
TRIAC&lt;br&gt;
Vdrm: 600V&lt;br&gt;
Itsm: 1A&lt;br&gt;
Viso: 5000V&lt;br&gt;
DV/dt: 1500V/us&lt;br&gt;
&lt;p&gt;
DIP: MOC3063M&lt;br&gt;
SMD: MOC3063MS &lt;a href="https://pricing.snapeda.com/parts/MOC3063M/Lite-On%20Inc./view-part?ref=eda"&gt;Check availability&lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="OPTOTRIAC_ZC" x="0" y="0"/>
</gates>
<devices>
<device name="S" package="DIP8SMD">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="K" pad="2"/>
<connect gate="G$1" pin="T1" pad="6"/>
<connect gate="G$1" pin="T2" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="In Stock"/>
<attribute name="DESCRIPTION" value=" Optoisolator Triac Output 5000Vrms 1 Channel 6-DIP "/>
<attribute name="MF" value="Lite-On Inc."/>
<attribute name="MP" value="MOC3063M"/>
<attribute name="PACKAGE" value="DIP-6 ON Semiconductor"/>
<attribute name="PRICE" value="None"/>
<attribute name="PURCHASE-URL" value="https://pricing.snapeda.com/search/part/MOC3063M/?ref=eda"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Resistors" urn="urn:adsk.eagle:library:532">
<description>&lt;h3&gt;SparkFun Resistors&lt;/h3&gt;
This library contains resistors. Reference designator:R. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="AXIAL-0.3" urn="urn:adsk.eagle:footprint:39622/1" library_version="1">
<description>&lt;h3&gt;AXIAL-0.3&lt;/h3&gt;
&lt;p&gt;Commonly used for 1/4W through-hole resistors. 0.3" pitch between holes.&lt;/p&gt;</description>
<wire x1="-2.54" y1="0.762" x2="2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="-2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="3.81" y="0" drill="0.9" diameter="1.8796"/>
<text x="0" y="1.016" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-1.016" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
<package name="AXIAL-0.3-KIT" urn="urn:adsk.eagle:footprint:39623/1" library_version="1">
<description>&lt;h3&gt;AXIAL-0.3-KIT&lt;/h3&gt;
&lt;p&gt;Commonly used for 1/4W through-hole resistors. 0.3" pitch between holes.&lt;/p&gt;
&lt;p&gt;&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of the AXIAL-0.3 package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.&lt;/p&gt;</description>
<wire x1="-2.54" y1="1.27" x2="2.54" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="0" width="0.254" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="-2.54" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="0" width="0.254" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.254" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.254" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="1.016" diameter="2.032" stop="no"/>
<pad name="P$2" x="3.81" y="0" drill="1.016" diameter="2.032" stop="no"/>
<text x="0" y="1.524" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.524" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<polygon width="0.127" layer="30">
<vertex x="3.8201" y="-0.9449" curve="-90"/>
<vertex x="2.8652" y="-0.0152" curve="-90.011749"/>
<vertex x="3.8176" y="0.9602" curve="-90"/>
<vertex x="4.7676" y="-0.0178" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="3.8176" y="-0.4369" curve="-90.012891"/>
<vertex x="3.3731" y="-0.0127" curve="-90"/>
<vertex x="3.8176" y="0.4546" curve="-90"/>
<vertex x="4.2595" y="-0.0025" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="-3.8075" y="-0.9525" curve="-90"/>
<vertex x="-4.7624" y="-0.0228" curve="-90.011749"/>
<vertex x="-3.81" y="0.9526" curve="-90"/>
<vertex x="-2.86" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-3.81" y="-0.4445" curve="-90.012891"/>
<vertex x="-4.2545" y="-0.0203" curve="-90"/>
<vertex x="-3.81" y="0.447" curve="-90"/>
<vertex x="-3.3681" y="-0.0101" curve="-90.012967"/>
</polygon>
</package>
<package name="0402" urn="urn:adsk.eagle:footprint:39625/1" library_version="1">
<description>&lt;p&gt;&lt;b&gt;Generic 1005 (0402) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<wire x1="-0.2704" y1="0.2286" x2="0.2704" y2="0.2286" width="0.1524" layer="51"/>
<wire x1="0.2704" y1="-0.2286" x2="-0.2704" y2="-0.2286" width="0.1524" layer="51"/>
<wire x1="-1.2" y1="0.65" x2="1.2" y2="0.65" width="0.0508" layer="39"/>
<wire x1="1.2" y1="0.65" x2="1.2" y2="-0.65" width="0.0508" layer="39"/>
<wire x1="1.2" y1="-0.65" x2="-1.2" y2="-0.65" width="0.0508" layer="39"/>
<wire x1="-1.2" y1="-0.65" x2="-1.2" y2="0.65" width="0.0508" layer="39"/>
<smd name="1" x="-0.58" y="0" dx="0.85" dy="0.9" layer="1"/>
<smd name="2" x="0.58" y="0" dx="0.85" dy="0.9" layer="1"/>
<text x="0" y="0.762" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.762" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.3048" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.3048" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="0603" urn="urn:adsk.eagle:footprint:39615/1" library_version="1">
<description>&lt;p&gt;&lt;b&gt;Generic 1608 (0603) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<wire x1="-1.6" y1="0.7" x2="1.6" y2="0.7" width="0.0508" layer="39"/>
<wire x1="1.6" y1="0.7" x2="1.6" y2="-0.7" width="0.0508" layer="39"/>
<wire x1="1.6" y1="-0.7" x2="-1.6" y2="-0.7" width="0.0508" layer="39"/>
<wire x1="-1.6" y1="-0.7" x2="-1.6" y2="0.7" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="0" y="0.762" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.762" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="0805" urn="urn:adsk.eagle:footprint:39617/1" library_version="1">
<description>&lt;p&gt;&lt;b&gt;Generic 2012 (0805) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<smd name="1" x="-0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<text x="0" y="0.889" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.889" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="-1.5" y1="0.8" x2="1.5" y2="0.8" width="0.0508" layer="39"/>
<wire x1="1.5" y1="0.8" x2="1.5" y2="-0.8" width="0.0508" layer="39"/>
<wire x1="1.5" y1="-0.8" x2="-1.5" y2="-0.8" width="0.0508" layer="39"/>
<wire x1="-1.5" y1="-0.8" x2="-1.5" y2="0.8" width="0.0508" layer="39"/>
</package>
<package name="1206" urn="urn:adsk.eagle:footprint:39619/1" library_version="1">
<description>&lt;p&gt;&lt;b&gt;Generic 3216 (1206) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<wire x1="-2.4" y1="1.1" x2="2.4" y2="1.1" width="0.0508" layer="39"/>
<wire x1="2.4" y1="-1.1" x2="-2.4" y2="-1.1" width="0.0508" layer="39"/>
<wire x1="-2.4" y1="-1.1" x2="-2.4" y2="1.1" width="0.0508" layer="39"/>
<wire x1="2.4" y1="1.1" x2="2.4" y2="-1.1" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="0" y="1.143" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.143" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
</packages>
<packages3d>
<package3d name="AXIAL-0.3" urn="urn:adsk.eagle:package:39658/1" type="box" library_version="1">
<description>AXIAL-0.3
Commonly used for 1/4W through-hole resistors. 0.3" pitch between holes.</description>
<packageinstances>
<packageinstance name="AXIAL-0.3"/>
</packageinstances>
</package3d>
<package3d name="AXIAL-0.3-KIT" urn="urn:adsk.eagle:package:39661/1" type="box" library_version="1">
<description>AXIAL-0.3-KIT
Commonly used for 1/4W through-hole resistors. 0.3" pitch between holes.
Warning: This is the KIT version of the AXIAL-0.3 package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<packageinstances>
<packageinstance name="AXIAL-0.3-KIT"/>
</packageinstances>
</package3d>
<package3d name="0402" urn="urn:adsk.eagle:package:39657/1" type="box" library_version="1">
<description>Generic 1005 (0402) package
0.2mm courtyard excess rounded to nearest 0.05mm.</description>
<packageinstances>
<packageinstance name="0402"/>
</packageinstances>
</package3d>
<package3d name="0603" urn="urn:adsk.eagle:package:39650/1" type="box" library_version="1">
<description>Generic 1608 (0603) package
0.2mm courtyard excess rounded to nearest 0.05mm.</description>
<packageinstances>
<packageinstance name="0603"/>
</packageinstances>
</package3d>
<package3d name="0805" urn="urn:adsk.eagle:package:39651/1" type="box" library_version="1">
<description>Generic 2012 (0805) package
0.2mm courtyard excess rounded to nearest 0.05mm.</description>
<packageinstances>
<packageinstance name="0805"/>
</packageinstances>
</package3d>
<package3d name="1206" urn="urn:adsk.eagle:package:39654/1" type="box" library_version="1">
<description>Generic 3216 (1206) package
0.2mm courtyard excess rounded to nearest 0.05mm.</description>
<packageinstances>
<packageinstance name="1206"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="RESISTOR" urn="urn:adsk.eagle:symbol:39614/1" library_version="1">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="0" y="1.524" size="1.778" layer="95" font="vector" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.524" size="1.778" layer="96" font="vector" align="top-center">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="RESISTOR" urn="urn:adsk.eagle:component:39766/1" prefix="R" library_version="1">
<description>Generic Resistor Package</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="AXIAL-0.3" package="AXIAL-0.3">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:39658/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value=" " constant="no"/>
<attribute name="VALUE" value=" " constant="no"/>
</technology>
</technologies>
</device>
<device name="AXIAL-0.3-KIT" package="AXIAL-0.3-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:39661/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value=" " constant="no"/>
<attribute name="VALUE" value=" " constant="no"/>
</technology>
</technologies>
</device>
<device name="0402" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:39657/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value=" " constant="no"/>
<attribute name="VALUE" value=" " constant="no"/>
</technology>
</technologies>
</device>
<device name="0603" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:39650/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value=" " constant="no"/>
<attribute name="VALUE" value=" " constant="no"/>
</technology>
</technologies>
</device>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:39651/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value=" " constant="no"/>
<attribute name="VALUE" value=" " constant="no"/>
</technology>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:39654/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value=" " constant="no"/>
<attribute name="VALUE" value=" " constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Wurth_Capacitors_WCAP-CSGP" urn="urn:adsk.eagle:library:18488985">
<description>&lt;BR&gt;Wurth Elektronik - Capacitors - MLCCs-Multilayer Ceramic Chip Capacitors - WCAP-CSGP&lt;br&gt;&lt;Hr&gt;

&lt;BR&gt;
&lt;TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0&gt;
&lt;TR&gt;   
&lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;&lt;BR&gt;&lt;br&gt;
      &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&lt;BR&gt;
       &lt;BR&gt;
       &lt;BR&gt;
       &lt;BR&gt;&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
&lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;&lt;br&gt;
      -----&lt;BR&gt;
      -----&lt;BR&gt;
      -----&lt;BR&gt;
      -----&lt;BR&gt;
      -----&lt;BR&gt;&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt; &lt;FONT FACE=ARIAL SIZE=3&gt;&lt;br&gt;
      ---------------------------&lt;BR&gt;
&lt;B&gt;&lt;I&gt;&lt;span style='font-size:26pt;
  color:#FF6600;'&gt;WE &lt;/span&gt;&lt;/i&gt;&lt;/b&gt;
&lt;BR&gt;
      ---------------------------&lt;BR&gt;&lt;b&gt;Würth Elektronik&lt;/b&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;&lt;br&gt;
      ---------O---&lt;BR&gt;
      ----O--------&lt;BR&gt;
      ---------O---&lt;BR&gt;
      ----O--------&lt;BR&gt;
      ---------O---&lt;BR&gt;&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
   
&lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;&lt;BR&gt;
      &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&lt;BR&gt;
       &lt;BR&gt;
       &lt;BR&gt;
       &lt;BR&gt;
       &lt;BR&gt;&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;

  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  
&lt;/TABLE&gt;
&lt;B&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;More than you expect&lt;BR&gt;&lt;BR&gt;&lt;BR&gt;&lt;/B&gt;

&lt;HR&gt;&lt;BR&gt;
&lt;b&gt;Würth Elektronik eiSos GmbH &amp; Co. KG&lt;/b&gt;&lt;br&gt;
EMC &amp; Inductive Solutions&lt;br&gt;

Max-Eyth-Str.1&lt;br&gt;
D-74638 Waldenburg&lt;br&gt;
&lt;br&gt;
Tel: +49 (0)7942-945-0&lt;br&gt;
Fax:+49 (0)7942-945-5000&lt;br&gt;
&lt;br&gt;
&lt;a href="http://www.we-online.com/web/en/electronic_components/produkte_pb/bauteilebibliotheken/eagle_4.php"&gt;www.we-online.com/eagle&lt;/a&gt;&lt;br&gt;
&lt;a href="mailto:libraries@we-online.com"&gt;libraries@we-online.com&lt;/a&gt; &lt;BR&gt;&lt;BR&gt;
&lt;br&gt;&lt;HR&gt;&lt;BR&gt;
Neither Autodesk nor Würth Elektronik eiSos does warrant that this library is error-free or &lt;br&gt;
that it meets your specific requirements.&lt;br&gt;&lt;BR&gt;
Please contact us for more information.&lt;br&gt;
&lt;HR&gt;
&lt;br&gt;Eagle Version 9, Library Revision 2022a, 2022-02-18&lt;br&gt;
&lt;HR&gt;
Copyright: Würth Elektronik</description>
<packages>
<package name="WCAP-CSGP_1210_H0.95" urn="urn:adsk.eagle:footprint:18489076/1" library_version="10">
<description>&lt;b&gt;WCAP-CSGP MLCCs 1210&lt;/b&gt;&lt;br&gt;3.2x2.5x0.95(LXWXH);</description>
<smd name="1" x="-1.55" y="0" dx="0.9" dy="2.5" layer="1"/>
<smd name="2" x="1.55" y="0" dx="0.9" dy="2.5" layer="1"/>
<wire x1="-1.6" y1="1.25" x2="-1.6" y2="-1.25" width="0.1" layer="51"/>
<wire x1="1.6" y1="-1.25" x2="1.6" y2="1.25" width="0.1" layer="51"/>
<wire x1="-1.6" y1="1.25" x2="1.6" y2="1.25" width="0.1" layer="51"/>
<wire x1="-1.6" y1="-1.25" x2="1.6" y2="-1.25" width="0.1" layer="51"/>
<wire x1="-0.8" y1="1.35" x2="0.8" y2="1.35" width="0.2" layer="21"/>
<wire x1="0.8" y1="-1.35" x2="-0.8" y2="-1.35" width="0.2" layer="21"/>
<text x="0" y="1.75" size="0.4064" layer="25" align="bottom-center">&gt;NAME</text>
<text x="-0.1" y="-2.2" size="0.4064" layer="27" align="bottom-center">&gt;VALUE</text>
<polygon width="0.1" layer="39">
<vertex x="-2.2" y="1.55"/>
<vertex x="2.2" y="1.55"/>
<vertex x="2.2" y="-1.55"/>
<vertex x="-2.2" y="-1.55"/>
</polygon>
</package>
</packages>
<packages3d>
<package3d name="WCAP-CSGP_1210_H0.95" urn="urn:adsk.eagle:package:18489123/2" type="model" library_version="10">
<description>&lt;b&gt;WCAP-CSGP MLCCs 1210&lt;/b&gt;&lt;br&gt;3.2x2.5x0.95(LXWXH);</description>
<packageinstances>
<packageinstance name="WCAP-CSGP_1210_H0.95"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="C" urn="urn:adsk.eagle:symbol:18489088/2" library_version="10">
<pin name="1" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<text x="1.175" y="3.38" size="1.4224" layer="95" align="bottom-center">&gt;NAME</text>
<text x="0.915" y="-5.075" size="1.4224" layer="96" align="bottom-center">&gt;VALUE</text>
<wire x1="1.905" y1="1.905" x2="1.905" y2="-1.905" width="0.254" layer="94"/>
<wire x1="0.635" y1="-1.905" x2="0.635" y2="1.905" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0.64261875" y2="0" width="0.1524" layer="94"/>
<wire x1="1.927859375" y1="0" x2="2.57048125" y2="0" width="0.1524" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="WCAP-CSGP_1210_H0.95" urn="urn:adsk.eagle:component:18489159/7" prefix="C" library_version="10">
<description>&lt;b&gt;WCAP-CSGP MLCCs 1210 &lt;/B&gt;&lt;BR&gt;&lt;BR&gt;

&lt;B&gt;General Purpose &lt;/B&gt;&lt;BR&gt;&lt;BR&gt;
&lt;B&gt;Characteristics&lt;/B&gt;&lt;BR&gt;
&lt;BR&gt;General purpose product series 
&lt;BR&gt;High performance ceramic 
&lt;BR&gt;High electrical precision and stability ceramic especially for NP0 
&lt;BR&gt;Minimal aging especially for NP0 
&lt;BR&gt;Mounting style: SMT-Chip 
&lt;BR&gt;Ceramic: NP0 (Class I), X5R (Class II), X7R (Class II) 
&lt;BR&gt;Capacitance range: 22 pF ‒ 100 µF 
&lt;BR&gt;Temperature Coefficient: 
&lt;BR&gt;±30 ppm/°C; ±0.54 % for NP0 
&lt;BR&gt;±15 % for X7R and X5R 
&lt;BR&gt;Voltage range (UR): 6.3 ‒ 100 V(DC) 
&lt;BR&gt;Operating temperature: 
&lt;BR&gt;-55 °C up to + 125 °C for NP0 and X7R 
&lt;BR&gt;-55 °C up to + 85 °C for X5R 
&lt;BR&gt;Termination: Cu/Ni/Sn 
&lt;BR&gt;Recommended soldering: Reflow soldering 
&lt;BR&gt;
&lt;BR&gt;
&lt;B&gt;Applications&lt;/B&gt;&lt;BR&gt;
&lt;BR&gt;Temperature compensation (NP0) 
&lt;BR&gt;Coupling 
&lt;BR&gt;Decoupling 
&lt;BR&gt;Bypass 
&lt;BR&gt;Smoothing 
&lt;BR&gt;Filtering 
&lt;BR&gt;Resonant circuit 
&lt;BR&gt;
&lt;BR&gt;&lt;BR&gt;
&lt;BR&gt;
&lt;br&gt;&lt;a href="https://www.we-online.com/catalog/media/o177837v209%20MLCC_GRP_all.jpg" title="Enlarge picture"&gt;
&lt;img src="https://www.we-online.com/catalog/media/o177837v209%20MLCC_GRP_all.jpg"  width="320"&gt;&lt;/a&gt;&lt;p&gt;

&lt;/b&gt;&lt;br&gt;            
Details see: &lt;a href="https://www.we-online.com/catalog/en/pbs/capacitors/multilayer_ceramic_chip_capacitors/mlcc_general_purpose"&gt;https://www.we-online.com/catalog/en/pbs/capacitors/multilayer_ceramic_chip_capacitors/mlcc_general_purpose&lt;/a&gt;&lt;p&gt;
&lt;br&gt;Updated by yingchun,Shan;2020-03-09
&lt;br&gt;2020(C) Würth Elektronik</description>
<gates>
<gate name="G$1" symbol="C" x="0" y="0"/>
</gates>
<devices>
<device name="_25V(DC)" package="WCAP-CSGP_1210_H0.95">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:18489123/2"/>
</package3dinstances>
<technologies>
<technology name="_885012009002">
<attribute name="CERAMIC-TYPE" value="NP0 Class I"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/885012009002.pdf"/>
<attribute name="DF" value="-"/>
<attribute name="H" value="0.95mm"/>
<attribute name="OPERATING-TEMPERATURE" value="-55 up to +125 °C"/>
<attribute name="PART-NUMBER" value=" 885012009002 "/>
<attribute name="QMIN" value=" 1000 "/>
<attribute name="RISO" value="10GOhm"/>
<attribute name="SIZE" value="1210"/>
<attribute name="TECHNICAL-REFERENCE" value="NP01210102J025DFCT10000"/>
<attribute name="VALUE" value="1nF"/>
<attribute name="VR" value="25V(DC)"/>
</technology>
<technology name="_885012009003">
<attribute name="CERAMIC-TYPE" value="NP0 Class I"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/885012009003.pdf"/>
<attribute name="DF" value="-"/>
<attribute name="H" value="0.95mm"/>
<attribute name="OPERATING-TEMPERATURE" value="-55 up to +125 °C"/>
<attribute name="PART-NUMBER" value=" 885012009003 "/>
<attribute name="QMIN" value=" 1000 "/>
<attribute name="RISO" value="10GOhm"/>
<attribute name="SIZE" value="1210"/>
<attribute name="TECHNICAL-REFERENCE" value="NP01210222J025DFCT10000"/>
<attribute name="VALUE" value="2.2nF"/>
<attribute name="VR" value="25V(DC)"/>
</technology>
<technology name="_885012009004">
<attribute name="CERAMIC-TYPE" value="NP0 Class I"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/885012009004.pdf"/>
<attribute name="DF" value="-"/>
<attribute name="H" value="0.95mm"/>
<attribute name="OPERATING-TEMPERATURE" value="-55 up to +125 °C"/>
<attribute name="PART-NUMBER" value=" 885012009004 "/>
<attribute name="QMIN" value=" 1000 "/>
<attribute name="RISO" value="10GOhm"/>
<attribute name="SIZE" value="1210"/>
<attribute name="TECHNICAL-REFERENCE" value="NP01210472J025DFCT10000"/>
<attribute name="VALUE" value="4.7nF"/>
<attribute name="VR" value="25V(DC)"/>
</technology>
<technology name="_885012009005">
<attribute name="CERAMIC-TYPE" value="NP0 Class I"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/885012009005.pdf"/>
<attribute name="DF" value="-"/>
<attribute name="H" value="0.95mm"/>
<attribute name="OPERATING-TEMPERATURE" value="-55 up to +125 °C"/>
<attribute name="PART-NUMBER" value=" 885012009005 "/>
<attribute name="QMIN" value=" 1000 "/>
<attribute name="RISO" value="10GOhm"/>
<attribute name="SIZE" value="1210"/>
<attribute name="TECHNICAL-REFERENCE" value="NP01210682J025DFCT10000"/>
<attribute name="VALUE" value="6.8nF"/>
<attribute name="VR" value="25V(DC)"/>
</technology>
<technology name="_885012209015">
<attribute name="CERAMIC-TYPE" value="X7R Class II"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/885012209015.pdf"/>
<attribute name="DF" value=" 0.035 "/>
<attribute name="H" value="0.95mm"/>
<attribute name="OPERATING-TEMPERATURE" value="-55 up to +125 °C"/>
<attribute name="PART-NUMBER" value=" 885012209015 "/>
<attribute name="QMIN" value="-"/>
<attribute name="RISO" value="10GOhm"/>
<attribute name="SIZE" value="1210"/>
<attribute name="TECHNICAL-REFERENCE" value="X7R1210102K025DFCT10000"/>
<attribute name="VALUE" value="1nF"/>
<attribute name="VR" value="25V(DC)"/>
</technology>
<technology name="_885012209016">
<attribute name="CERAMIC-TYPE" value="X7R Class II"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/885012209016.pdf"/>
<attribute name="DF" value=" 0.035 "/>
<attribute name="H" value="0.95mm"/>
<attribute name="OPERATING-TEMPERATURE" value="-55 up to +125 °C"/>
<attribute name="PART-NUMBER" value=" 885012209016 "/>
<attribute name="QMIN" value="-"/>
<attribute name="RISO" value="10GOhm"/>
<attribute name="SIZE" value="1210"/>
<attribute name="TECHNICAL-REFERENCE" value="X7R1210222K025DFCT10000"/>
<attribute name="VALUE" value="2.2nF"/>
<attribute name="VR" value="25V(DC)"/>
</technology>
<technology name="_885012209017">
<attribute name="CERAMIC-TYPE" value="X7R Class II"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/885012209017.pdf"/>
<attribute name="DF" value=" 0.035 "/>
<attribute name="H" value="0.95mm"/>
<attribute name="OPERATING-TEMPERATURE" value="-55 up to +125 °C"/>
<attribute name="PART-NUMBER" value=" 885012209017 "/>
<attribute name="QMIN" value="-"/>
<attribute name="RISO" value="10GOhm"/>
<attribute name="SIZE" value="1210"/>
<attribute name="TECHNICAL-REFERENCE" value="X7R1210103K025DFCT10000"/>
<attribute name="VALUE" value="10nF"/>
<attribute name="VR" value="25V(DC)"/>
</technology>
<technology name="_885012209018">
<attribute name="CERAMIC-TYPE" value="X7R Class II"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/885012209018.pdf"/>
<attribute name="DF" value=" 0.035 "/>
<attribute name="H" value="0.95mm"/>
<attribute name="OPERATING-TEMPERATURE" value="-55 up to +125 °C"/>
<attribute name="PART-NUMBER" value=" 885012209018 "/>
<attribute name="QMIN" value="-"/>
<attribute name="RISO" value="10GOhm"/>
<attribute name="SIZE" value="1210"/>
<attribute name="TECHNICAL-REFERENCE" value="X7R1210223K025DFCT10000"/>
<attribute name="VALUE" value="22nF"/>
<attribute name="VR" value="25V(DC)"/>
</technology>
<technology name="_885012209019">
<attribute name="CERAMIC-TYPE" value="X7R Class II"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/885012209019.pdf"/>
<attribute name="DF" value=" 0.035 "/>
<attribute name="H" value="0.95mm"/>
<attribute name="OPERATING-TEMPERATURE" value="-55 up to +125 °C"/>
<attribute name="PART-NUMBER" value=" 885012209019 "/>
<attribute name="QMIN" value="-"/>
<attribute name="RISO" value="5GOhm"/>
<attribute name="SIZE" value="1210"/>
<attribute name="TECHNICAL-REFERENCE" value="X7R1210104K025DFCT10000"/>
<attribute name="VALUE" value="100nF"/>
<attribute name="VR" value="25V(DC)"/>
</technology>
<technology name="_885012209020">
<attribute name="CERAMIC-TYPE" value="X7R Class II"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/885012209020.pdf"/>
<attribute name="DF" value=" 0.035 "/>
<attribute name="H" value="0.95mm"/>
<attribute name="OPERATING-TEMPERATURE" value="-55 up to +125 °C"/>
<attribute name="PART-NUMBER" value=" 885012209020 "/>
<attribute name="QMIN" value="-"/>
<attribute name="RISO" value="2.3GOhm"/>
<attribute name="SIZE" value="1210"/>
<attribute name="TECHNICAL-REFERENCE" value="X7R1210224K025DFCT10000"/>
<attribute name="VALUE" value="220nF"/>
<attribute name="VR" value="25V(DC)"/>
</technology>
<technology name="_885012209021">
<attribute name="CERAMIC-TYPE" value="X7R Class II"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/885012209021.pdf"/>
<attribute name="DF" value=" 0.035 "/>
<attribute name="H" value="0.95mm"/>
<attribute name="OPERATING-TEMPERATURE" value="-55 up to +125 °C"/>
<attribute name="PART-NUMBER" value=" 885012209021 "/>
<attribute name="QMIN" value="-"/>
<attribute name="RISO" value="1.5GOhm"/>
<attribute name="SIZE" value="1210"/>
<attribute name="TECHNICAL-REFERENCE" value="X7R1210334K025DFCT10000"/>
<attribute name="VALUE" value="330nF"/>
<attribute name="VR" value="25V(DC)"/>
</technology>
<technology name="_885012209022">
<attribute name="CERAMIC-TYPE" value="X7R Class II"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/885012209022.pdf"/>
<attribute name="DF" value=" 0.035 "/>
<attribute name="H" value="0.95mm"/>
<attribute name="OPERATING-TEMPERATURE" value="-55 up to +125 °C"/>
<attribute name="PART-NUMBER" value=" 885012209022 "/>
<attribute name="QMIN" value="-"/>
<attribute name="RISO" value="1.1GOhm"/>
<attribute name="SIZE" value="1210"/>
<attribute name="TECHNICAL-REFERENCE" value="X7R1210474K025DFCT10000"/>
<attribute name="VALUE" value="470nF"/>
<attribute name="VR" value="25V(DC)"/>
</technology>
</technologies>
</device>
<device name="_10V(DC)" package="WCAP-CSGP_1210_H0.95">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:18489123/2"/>
</package3dinstances>
<technologies>
<technology name="_885012209001">
<attribute name="CERAMIC-TYPE" value="X7R Class II"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/885012209001.pdf"/>
<attribute name="DF" value=" 0.05 "/>
<attribute name="H" value="0.95mm"/>
<attribute name="OPERATING-TEMPERATURE" value="-55 up to +125 °C"/>
<attribute name="PART-NUMBER" value=" 885012209001 "/>
<attribute name="QMIN" value="-"/>
<attribute name="RISO" value="10GOhm"/>
<attribute name="SIZE" value="1210"/>
<attribute name="TECHNICAL-REFERENCE" value="X7R1210473K010DFCT10000"/>
<attribute name="VALUE" value="47nF"/>
<attribute name="VR" value="10V(DC)"/>
</technology>
<technology name="_885012209002">
<attribute name="CERAMIC-TYPE" value="X7R Class II"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/885012209002.pdf"/>
<attribute name="DF" value=" 0.05 "/>
<attribute name="H" value="0.95mm"/>
<attribute name="OPERATING-TEMPERATURE" value="-55 up to +125 °C"/>
<attribute name="PART-NUMBER" value=" 885012209002 "/>
<attribute name="QMIN" value="-"/>
<attribute name="RISO" value="2.3GOhm"/>
<attribute name="SIZE" value="1210"/>
<attribute name="TECHNICAL-REFERENCE" value="X7R1210224K010DFCT10000"/>
<attribute name="VALUE" value="220nF"/>
<attribute name="VR" value="10V(DC)"/>
</technology>
</technologies>
</device>
<device name="_16V(DC)" package="WCAP-CSGP_1210_H0.95">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:18489123/2"/>
</package3dinstances>
<technologies>
<technology name="_885012209007">
<attribute name="CERAMIC-TYPE" value="X7R Class II"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/885012209007.pdf"/>
<attribute name="DF" value=" 0.035 "/>
<attribute name="H" value="0.95mm"/>
<attribute name="OPERATING-TEMPERATURE" value="-55 up to +125 °C"/>
<attribute name="PART-NUMBER" value=" 885012209007 "/>
<attribute name="QMIN" value="-"/>
<attribute name="RISO" value="3.3GOhm"/>
<attribute name="SIZE" value="1210"/>
<attribute name="TECHNICAL-REFERENCE" value="X7R1210154K016DFCT10000"/>
<attribute name="VALUE" value="150nF"/>
<attribute name="VR" value="16V(DC)"/>
</technology>
<technology name="_885012209008">
<attribute name="CERAMIC-TYPE" value="X7R Class II"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/885012209008.pdf"/>
<attribute name="DF" value=" 0.035 "/>
<attribute name="H" value="0.95mm"/>
<attribute name="OPERATING-TEMPERATURE" value="-55 up to +125 °C"/>
<attribute name="PART-NUMBER" value=" 885012209008 "/>
<attribute name="QMIN" value="-"/>
<attribute name="RISO" value="2.3GOhm"/>
<attribute name="SIZE" value="1210"/>
<attribute name="TECHNICAL-REFERENCE" value="X7R1210224K016DFCT10000"/>
<attribute name="VALUE" value="220nF"/>
<attribute name="VR" value="16V(DC)"/>
</technology>
<technology name="_885012209009">
<attribute name="CERAMIC-TYPE" value="X7R Class II"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/885012209009.pdf"/>
<attribute name="DF" value=" 0.035 "/>
<attribute name="H" value="0.95mm"/>
<attribute name="OPERATING-TEMPERATURE" value="-55 up to +125 °C"/>
<attribute name="PART-NUMBER" value=" 885012209009 "/>
<attribute name="QMIN" value="-"/>
<attribute name="RISO" value="1.1GOhm"/>
<attribute name="SIZE" value="1210"/>
<attribute name="TECHNICAL-REFERENCE" value="X7R1210474K016DFCT10000"/>
<attribute name="VALUE" value="470nF"/>
<attribute name="VR" value="16V(DC)"/>
</technology>
</technologies>
</device>
<device name="_50V(DC)" package="WCAP-CSGP_1210_H0.95">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:18489123/2"/>
</package3dinstances>
<technologies>
<technology name="_885012009007">
<attribute name="CERAMIC-TYPE" value="NP0 Class I"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/885012009007.pdf"/>
<attribute name="DF" value="-"/>
<attribute name="H" value="0.95mm"/>
<attribute name="OPERATING-TEMPERATURE" value="-55 up to +125 °C"/>
<attribute name="PART-NUMBER" value=" 885012009007 "/>
<attribute name="QMIN" value="400+20C"/>
<attribute name="RISO" value="10GOhm"/>
<attribute name="SIZE" value="1210"/>
<attribute name="TECHNICAL-REFERENCE" value="NP01210220J050DFCT10000"/>
<attribute name="VALUE" value="22pF"/>
<attribute name="VR" value="50V(DC)"/>
</technology>
<technology name="_885012009008">
<attribute name="CERAMIC-TYPE" value="NP0 Class I"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/885012009008.pdf"/>
<attribute name="DF" value="-"/>
<attribute name="H" value="0.95mm"/>
<attribute name="OPERATING-TEMPERATURE" value="-55 up to +125 °C"/>
<attribute name="PART-NUMBER" value=" 885012009008 "/>
<attribute name="QMIN" value=" 1000 "/>
<attribute name="RISO" value="10GOhm"/>
<attribute name="SIZE" value="1210"/>
<attribute name="TECHNICAL-REFERENCE" value="NP01210330J050DFCT10000"/>
<attribute name="VALUE" value="33pF"/>
<attribute name="VR" value="50V(DC)"/>
</technology>
<technology name="_885012009009">
<attribute name="CERAMIC-TYPE" value="NP0 Class I"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/885012009009.pdf"/>
<attribute name="DF" value="-"/>
<attribute name="H" value="0.95mm"/>
<attribute name="OPERATING-TEMPERATURE" value="-55 up to +125 °C"/>
<attribute name="PART-NUMBER" value=" 885012009009 "/>
<attribute name="QMIN" value=" 1000 "/>
<attribute name="RISO" value="10GOhm"/>
<attribute name="SIZE" value="1210"/>
<attribute name="TECHNICAL-REFERENCE" value="NP01210470J050DFCT10000"/>
<attribute name="VALUE" value="47pF"/>
<attribute name="VR" value="50V(DC)"/>
</technology>
<technology name="_885012009010">
<attribute name="CERAMIC-TYPE" value="NP0 Class I"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/885012009010.pdf"/>
<attribute name="DF" value="-"/>
<attribute name="H" value="0.95mm"/>
<attribute name="OPERATING-TEMPERATURE" value="-55 up to +125 °C"/>
<attribute name="PART-NUMBER" value=" 885012009010 "/>
<attribute name="QMIN" value=" 1000 "/>
<attribute name="RISO" value="10GOhm"/>
<attribute name="SIZE" value="1210"/>
<attribute name="TECHNICAL-REFERENCE" value="NP01210680J050DFCT10000"/>
<attribute name="VALUE" value="68pF"/>
<attribute name="VR" value="50V(DC)"/>
</technology>
<technology name="_885012009011">
<attribute name="CERAMIC-TYPE" value="NP0 Class I"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/885012009011.pdf"/>
<attribute name="DF" value="-"/>
<attribute name="H" value="0.95mm"/>
<attribute name="OPERATING-TEMPERATURE" value="-55 up to +125 °C"/>
<attribute name="PART-NUMBER" value=" 885012009011 "/>
<attribute name="QMIN" value=" 1000 "/>
<attribute name="RISO" value="10GOhm"/>
<attribute name="SIZE" value="1210"/>
<attribute name="TECHNICAL-REFERENCE" value="NP01210101J050DFCT10000"/>
<attribute name="VALUE" value="100pF"/>
<attribute name="VR" value="50V(DC)"/>
</technology>
<technology name="_885012009012">
<attribute name="CERAMIC-TYPE" value="NP0 Class I"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/885012009012.pdf"/>
<attribute name="DF" value="-"/>
<attribute name="H" value="0.95mm"/>
<attribute name="OPERATING-TEMPERATURE" value="-55 up to +125 °C"/>
<attribute name="PART-NUMBER" value=" 885012009012 "/>
<attribute name="QMIN" value=" 1000 "/>
<attribute name="RISO" value="10GOhm"/>
<attribute name="SIZE" value="1210"/>
<attribute name="TECHNICAL-REFERENCE" value="NP01210151J050DFCT10000"/>
<attribute name="VALUE" value="150pF"/>
<attribute name="VR" value="50V(DC)"/>
</technology>
<technology name="_885012009013">
<attribute name="CERAMIC-TYPE" value="NP0 Class I"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/885012009013.pdf"/>
<attribute name="DF" value="-"/>
<attribute name="H" value="0.95mm"/>
<attribute name="OPERATING-TEMPERATURE" value="-55 up to +125 °C"/>
<attribute name="PART-NUMBER" value=" 885012009013 "/>
<attribute name="QMIN" value=" 1000 "/>
<attribute name="RISO" value="10GOhm"/>
<attribute name="SIZE" value="1210"/>
<attribute name="TECHNICAL-REFERENCE" value="NP01210221J050DFCT10000"/>
<attribute name="VALUE" value="220pF"/>
<attribute name="VR" value="50V(DC)"/>
</technology>
<technology name="_885012009014">
<attribute name="CERAMIC-TYPE" value="NP0 Class I"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/885012009014.pdf"/>
<attribute name="DF" value="-"/>
<attribute name="H" value="0.95mm"/>
<attribute name="OPERATING-TEMPERATURE" value="-55 up to +125 °C"/>
<attribute name="PART-NUMBER" value=" 885012009014 "/>
<attribute name="QMIN" value=" 1000 "/>
<attribute name="RISO" value="10GOhm"/>
<attribute name="SIZE" value="1210"/>
<attribute name="TECHNICAL-REFERENCE" value="NP01210331J050DFCT10000"/>
<attribute name="VALUE" value="330pF"/>
<attribute name="VR" value="50V(DC)"/>
</technology>
<technology name="_885012009015">
<attribute name="CERAMIC-TYPE" value="NP0 Class I"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/885012009015.pdf"/>
<attribute name="DF" value="-"/>
<attribute name="H" value="0.95mm"/>
<attribute name="OPERATING-TEMPERATURE" value="-55 up to +125 °C"/>
<attribute name="PART-NUMBER" value=" 885012009015 "/>
<attribute name="QMIN" value=" 1000 "/>
<attribute name="RISO" value="10GOhm"/>
<attribute name="SIZE" value="1210"/>
<attribute name="TECHNICAL-REFERENCE" value="NP01210471J050DFCT10000"/>
<attribute name="VALUE" value="470pF"/>
<attribute name="VR" value="50V(DC)"/>
</technology>
<technology name="_885012009016">
<attribute name="CERAMIC-TYPE" value="NP0 Class I"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/885012009016.pdf"/>
<attribute name="DF" value="-"/>
<attribute name="H" value="0.95mm"/>
<attribute name="OPERATING-TEMPERATURE" value="-55 up to +125 °C"/>
<attribute name="PART-NUMBER" value=" 885012009016 "/>
<attribute name="QMIN" value=" 1000 "/>
<attribute name="RISO" value="10GOhm"/>
<attribute name="SIZE" value="1210"/>
<attribute name="TECHNICAL-REFERENCE" value="NP01210681J050DFCT10000"/>
<attribute name="VALUE" value="680pF"/>
<attribute name="VR" value="50V(DC)"/>
</technology>
<technology name="_885012009017">
<attribute name="CERAMIC-TYPE" value="NP0 Class I"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/885012009017.pdf"/>
<attribute name="DF" value="-"/>
<attribute name="H" value="0.95mm"/>
<attribute name="OPERATING-TEMPERATURE" value="-55 up to +125 °C"/>
<attribute name="PART-NUMBER" value=" 885012009017 "/>
<attribute name="QMIN" value=" 1000 "/>
<attribute name="RISO" value="10GOhm"/>
<attribute name="SIZE" value="1210"/>
<attribute name="TECHNICAL-REFERENCE" value="NP01210102J050DFCT10000"/>
<attribute name="VALUE" value="1nF"/>
<attribute name="VR" value="50V(DC)"/>
</technology>
<technology name="_885012009018">
<attribute name="CERAMIC-TYPE" value="NP0 Class I"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/885012009018.pdf"/>
<attribute name="DF" value="-"/>
<attribute name="H" value="0.95mm"/>
<attribute name="OPERATING-TEMPERATURE" value="-55 up to +125 °C"/>
<attribute name="PART-NUMBER" value=" 885012009018 "/>
<attribute name="QMIN" value=" 1000 "/>
<attribute name="RISO" value="10GOhm"/>
<attribute name="SIZE" value="1210"/>
<attribute name="TECHNICAL-REFERENCE" value="NP01210152J050DFCT10000"/>
<attribute name="VALUE" value="1.5nF"/>
<attribute name="VR" value="50V(DC)"/>
</technology>
<technology name="_885012009019">
<attribute name="CERAMIC-TYPE" value="NP0 Class I"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/885012009019.pdf"/>
<attribute name="DF" value="-"/>
<attribute name="H" value="0.95mm"/>
<attribute name="OPERATING-TEMPERATURE" value="-55 up to +125 °C"/>
<attribute name="PART-NUMBER" value=" 885012009019 "/>
<attribute name="QMIN" value=" 1000 "/>
<attribute name="RISO" value="10GOhm"/>
<attribute name="SIZE" value="1210"/>
<attribute name="TECHNICAL-REFERENCE" value="NP01210222J050DFCT10000"/>
<attribute name="VALUE" value="2.2nF"/>
<attribute name="VR" value="50V(DC)"/>
</technology>
<technology name="_885012009020">
<attribute name="CERAMIC-TYPE" value="NP0 Class I"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/885012009020.pdf"/>
<attribute name="DF" value="-"/>
<attribute name="H" value="0.95mm"/>
<attribute name="OPERATING-TEMPERATURE" value="-55 up to +125 °C"/>
<attribute name="PART-NUMBER" value=" 885012009020 "/>
<attribute name="QMIN" value=" 1000 "/>
<attribute name="RISO" value="10GOhm"/>
<attribute name="SIZE" value="1210"/>
<attribute name="TECHNICAL-REFERENCE" value="NP01210332J050DFCT10000"/>
<attribute name="VALUE" value="3.3nF"/>
<attribute name="VR" value="50V(DC)"/>
</technology>
<technology name="_885012009021">
<attribute name="CERAMIC-TYPE" value="NP0 Class I"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/885012009021.pdf"/>
<attribute name="DF" value="-"/>
<attribute name="H" value="0.95mm"/>
<attribute name="OPERATING-TEMPERATURE" value="-55 up to +125 °C"/>
<attribute name="PART-NUMBER" value=" 885012009021 "/>
<attribute name="QMIN" value=" 1000 "/>
<attribute name="RISO" value="10GOhm"/>
<attribute name="SIZE" value="1210"/>
<attribute name="TECHNICAL-REFERENCE" value="NP01210472J050DFCT10000"/>
<attribute name="VALUE" value="4.7nF"/>
<attribute name="VR" value="50V(DC)"/>
</technology>
<technology name="_885012009022">
<attribute name="CERAMIC-TYPE" value="NP0 Class I"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/885012009022.pdf"/>
<attribute name="DF" value="-"/>
<attribute name="H" value="0.95mm"/>
<attribute name="OPERATING-TEMPERATURE" value="-55 up to +125 °C"/>
<attribute name="PART-NUMBER" value=" 885012009022 "/>
<attribute name="QMIN" value=" 1000 "/>
<attribute name="RISO" value="10GOhm"/>
<attribute name="SIZE" value="1210"/>
<attribute name="TECHNICAL-REFERENCE" value="NP01210682J050DFCT10000"/>
<attribute name="VALUE" value="6.8nF"/>
<attribute name="VR" value="50V(DC)"/>
</technology>
<technology name="_885012009023">
<attribute name="CERAMIC-TYPE" value="NP0 Class I"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/885012009023.pdf"/>
<attribute name="DF" value="-"/>
<attribute name="H" value="0.95mm"/>
<attribute name="OPERATING-TEMPERATURE" value="-55 up to +125 °C"/>
<attribute name="PART-NUMBER" value=" 885012009023 "/>
<attribute name="QMIN" value=" 1000 "/>
<attribute name="RISO" value="10GOhm"/>
<attribute name="SIZE" value="1210"/>
<attribute name="TECHNICAL-REFERENCE" value="NP01210103J050DFCT10000"/>
<attribute name="VALUE" value="10nF"/>
<attribute name="VR" value="50V(DC)"/>
</technology>
<technology name="_885012209029">
<attribute name="CERAMIC-TYPE" value="X7R Class II"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/885012209029.pdf"/>
<attribute name="DF" value=" 0.025 "/>
<attribute name="H" value="0.95mm"/>
<attribute name="OPERATING-TEMPERATURE" value="-55 up to +125 °C"/>
<attribute name="PART-NUMBER" value=" 885012209029 "/>
<attribute name="QMIN" value="-"/>
<attribute name="RISO" value="10GOhm"/>
<attribute name="SIZE" value="1210"/>
<attribute name="TECHNICAL-REFERENCE" value="X7R1210102K050DFCT10000"/>
<attribute name="VALUE" value="1nF"/>
<attribute name="VR" value="50V(DC)"/>
</technology>
<technology name="_885012209030">
<attribute name="CERAMIC-TYPE" value="X7R Class II"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/885012209030.pdf"/>
<attribute name="DF" value=" 0.025 "/>
<attribute name="H" value="0.95mm"/>
<attribute name="OPERATING-TEMPERATURE" value="-55 up to +125 °C"/>
<attribute name="PART-NUMBER" value=" 885012209030 "/>
<attribute name="QMIN" value="-"/>
<attribute name="RISO" value="10GOhm"/>
<attribute name="SIZE" value="1210"/>
<attribute name="TECHNICAL-REFERENCE" value="X7R1210152K050DFCT10000"/>
<attribute name="VALUE" value="1nF"/>
<attribute name="VR" value="50V(DC)"/>
</technology>
<technology name="_885012209031">
<attribute name="CERAMIC-TYPE" value="X7R Class II"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/885012209031.pdf"/>
<attribute name="DF" value=" 0.025 "/>
<attribute name="H" value="0.95mm"/>
<attribute name="OPERATING-TEMPERATURE" value="-55 up to +125 °C"/>
<attribute name="PART-NUMBER" value=" 885012209031 "/>
<attribute name="QMIN" value="-"/>
<attribute name="RISO" value="10GOhm"/>
<attribute name="SIZE" value="1210"/>
<attribute name="TECHNICAL-REFERENCE" value="X7R1210222K050DFCT10000"/>
<attribute name="VALUE" value="2.2nF"/>
<attribute name="VR" value="50V(DC)"/>
</technology>
<technology name="_885012209032">
<attribute name="CERAMIC-TYPE" value="X7R Class II"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/885012209032.pdf"/>
<attribute name="DF" value=" 0.025 "/>
<attribute name="H" value="0.95mm"/>
<attribute name="OPERATING-TEMPERATURE" value="-55 up to +125 °C"/>
<attribute name="PART-NUMBER" value=" 885012209032 "/>
<attribute name="QMIN" value="-"/>
<attribute name="RISO" value="10GOhm"/>
<attribute name="SIZE" value="1210"/>
<attribute name="TECHNICAL-REFERENCE" value="X7R1210332K050DFCT10000"/>
<attribute name="VALUE" value="3.3nF"/>
<attribute name="VR" value="50V(DC)"/>
</technology>
<technology name="_885012209033">
<attribute name="CERAMIC-TYPE" value="X7R Class II"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/885012209033.pdf"/>
<attribute name="DF" value=" 0.025 "/>
<attribute name="H" value="0.95mm"/>
<attribute name="OPERATING-TEMPERATURE" value="-55 up to +125 °C"/>
<attribute name="PART-NUMBER" value=" 885012209033 "/>
<attribute name="QMIN" value="-"/>
<attribute name="RISO" value="10GOhm"/>
<attribute name="SIZE" value="1210"/>
<attribute name="TECHNICAL-REFERENCE" value="X7R1210472K050DFCT10000"/>
<attribute name="VALUE" value="4.7nF"/>
<attribute name="VR" value="50V(DC)"/>
</technology>
<technology name="_885012209034">
<attribute name="CERAMIC-TYPE" value="X7R Class II"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/885012209034.pdf"/>
<attribute name="DF" value=" 0.025 "/>
<attribute name="H" value="0.95mm"/>
<attribute name="OPERATING-TEMPERATURE" value="-55 up to +125 °C"/>
<attribute name="PART-NUMBER" value=" 885012209034 "/>
<attribute name="QMIN" value="-"/>
<attribute name="RISO" value="10GOhm"/>
<attribute name="SIZE" value="1210"/>
<attribute name="TECHNICAL-REFERENCE" value="X7R1210682K050DFCT10000"/>
<attribute name="VALUE" value="6.8nF"/>
<attribute name="VR" value="50V(DC)"/>
</technology>
<technology name="_885012209035">
<attribute name="CERAMIC-TYPE" value="X7R Class II"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/885012209035.pdf"/>
<attribute name="DF" value=" 0.025 "/>
<attribute name="H" value="0.95mm"/>
<attribute name="OPERATING-TEMPERATURE" value="-55 up to +125 °C"/>
<attribute name="PART-NUMBER" value=" 885012209035 "/>
<attribute name="QMIN" value="-"/>
<attribute name="RISO" value="10GOhm"/>
<attribute name="SIZE" value="1210"/>
<attribute name="TECHNICAL-REFERENCE" value="X7R1210103K050DFCT10000"/>
<attribute name="VALUE" value="10nF"/>
<attribute name="VR" value="50V(DC)"/>
</technology>
<technology name="_885012209036">
<attribute name="CERAMIC-TYPE" value="X7R Class II"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/885012209036.pdf"/>
<attribute name="DF" value=" 0.025 "/>
<attribute name="H" value="0.95mm"/>
<attribute name="OPERATING-TEMPERATURE" value="-55 up to +125 °C"/>
<attribute name="PART-NUMBER" value=" 885012209036 "/>
<attribute name="QMIN" value="-"/>
<attribute name="RISO" value="10GOhm"/>
<attribute name="SIZE" value="1210"/>
<attribute name="TECHNICAL-REFERENCE" value="X7R1210153K050DFCT10000"/>
<attribute name="VALUE" value="15nF"/>
<attribute name="VR" value="50V(DC)"/>
</technology>
<technology name="_885012209037">
<attribute name="CERAMIC-TYPE" value="X7R Class II"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/885012209037.pdf"/>
<attribute name="DF" value=" 0.025 "/>
<attribute name="H" value="0.95mm"/>
<attribute name="OPERATING-TEMPERATURE" value="-55 up to +125 °C"/>
<attribute name="PART-NUMBER" value=" 885012209037 "/>
<attribute name="QMIN" value="-"/>
<attribute name="RISO" value="10GOhm"/>
<attribute name="SIZE" value="1210"/>
<attribute name="TECHNICAL-REFERENCE" value="X7R1210223K050DFCT10000"/>
<attribute name="VALUE" value="22nF"/>
<attribute name="VR" value="50V(DC)"/>
</technology>
<technology name="_885012209038">
<attribute name="CERAMIC-TYPE" value="X7R Class II"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/885012209038.pdf"/>
<attribute name="DF" value=" 0.025 "/>
<attribute name="H" value="0.95mm"/>
<attribute name="OPERATING-TEMPERATURE" value="-55 up to +125 °C"/>
<attribute name="PART-NUMBER" value=" 885012209038 "/>
<attribute name="QMIN" value="-"/>
<attribute name="RISO" value="10GOhm"/>
<attribute name="SIZE" value="1210"/>
<attribute name="TECHNICAL-REFERENCE" value="X7R1210333K050DFCT10000"/>
<attribute name="VALUE" value="33nF"/>
<attribute name="VR" value="50V(DC)"/>
</technology>
<technology name="_885012209039">
<attribute name="CERAMIC-TYPE" value="X7R Class II"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/885012209039.pdf"/>
<attribute name="DF" value=" 0.025 "/>
<attribute name="H" value="0.95mm"/>
<attribute name="OPERATING-TEMPERATURE" value="-55 up to +125 °C"/>
<attribute name="PART-NUMBER" value=" 885012209039 "/>
<attribute name="QMIN" value="-"/>
<attribute name="RISO" value="10GOhm"/>
<attribute name="SIZE" value="1210"/>
<attribute name="TECHNICAL-REFERENCE" value="X7R1210473K050DFCT10000"/>
<attribute name="VALUE" value="47nF"/>
<attribute name="VR" value="50V(DC)"/>
</technology>
<technology name="_885012209040">
<attribute name="CERAMIC-TYPE" value="X7R Class II"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/885012209040.pdf"/>
<attribute name="DF" value=" 0.025 "/>
<attribute name="H" value="0.95mm"/>
<attribute name="OPERATING-TEMPERATURE" value="-55 up to +125 °C"/>
<attribute name="PART-NUMBER" value=" 885012209040 "/>
<attribute name="QMIN" value="-"/>
<attribute name="RISO" value="7.4GOhm"/>
<attribute name="SIZE" value="1210"/>
<attribute name="TECHNICAL-REFERENCE" value="X7R1210683K050DFCT10000"/>
<attribute name="VALUE" value="68nF"/>
<attribute name="VR" value="50V(DC)"/>
</technology>
<technology name="_885012209041">
<attribute name="CERAMIC-TYPE" value="X7R Class II"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/885012209041.pdf"/>
<attribute name="DF" value=" 0.025 "/>
<attribute name="H" value="0.95mm"/>
<attribute name="OPERATING-TEMPERATURE" value="-55 up to +125 °C"/>
<attribute name="PART-NUMBER" value=" 885012209041 "/>
<attribute name="QMIN" value="-"/>
<attribute name="RISO" value="5GOhm"/>
<attribute name="SIZE" value="1210"/>
<attribute name="TECHNICAL-REFERENCE" value="X7R1210104K050DFCT10000"/>
<attribute name="VALUE" value="100nF"/>
<attribute name="VR" value="50V(DC)"/>
</technology>
<technology name="_885012209042">
<attribute name="CERAMIC-TYPE" value="X7R Class II"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/885012209042.pdf"/>
<attribute name="DF" value=" 0.025 "/>
<attribute name="H" value="0.95mm"/>
<attribute name="OPERATING-TEMPERATURE" value="-55 up to +125 °C"/>
<attribute name="PART-NUMBER" value=" 885012209042 "/>
<attribute name="QMIN" value="-"/>
<attribute name="RISO" value="3.3GOhm"/>
<attribute name="SIZE" value="1210"/>
<attribute name="TECHNICAL-REFERENCE" value="X7R1210154K050DFCT10000"/>
<attribute name="VALUE" value="150nF"/>
<attribute name="VR" value="50V(DC)"/>
</technology>
<technology name="_885012209043">
<attribute name="CERAMIC-TYPE" value="X7R Class II"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/885012209043.pdf"/>
<attribute name="DF" value=" 0.025 "/>
<attribute name="H" value="0.95mm"/>
<attribute name="OPERATING-TEMPERATURE" value="-55 up to +125 °C"/>
<attribute name="PART-NUMBER" value=" 885012209043 "/>
<attribute name="QMIN" value="-"/>
<attribute name="RISO" value="2.3GOhm"/>
<attribute name="SIZE" value="1210"/>
<attribute name="TECHNICAL-REFERENCE" value="X7R1210224K050DFCT10000"/>
<attribute name="VALUE" value="220nF"/>
<attribute name="VR" value="50V(DC)"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Power_Symbols" urn="urn:adsk.eagle:library:16502351">
<description>&lt;B&gt;Supply &amp; Ground symbols</description>
<packages>
</packages>
<symbols>
<symbol name="DGND" urn="urn:adsk.eagle:symbol:18498241/2" library_version="9">
<description>Digital Ground (DGND) Arrow</description>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-0.127" y="-3.683" size="1.778" layer="96" align="bottom-center">&gt;VALUE</text>
<pin name="DGND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="5V0-CIRCLE" urn="urn:adsk.eagle:symbol:18498235/2" library_version="9">
<description>5 Volt (5V0) Circle</description>
<circle x="0" y="1.27" radius="1.27" width="0.254" layer="94"/>
<text x="-0.127" y="3.175" size="1.778" layer="96" align="bottom-center">&gt;VALUE</text>
<pin name="5V0" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="3V3-CIRCLE" urn="urn:adsk.eagle:symbol:18498229/2" library_version="9">
<description>3.3 Volt (3V3) Circle</description>
<circle x="0" y="1.27" radius="1.27" width="0.254" layer="94"/>
<text x="-0.127" y="3.175" size="1.778" layer="96" align="bottom-center">&gt;VALUE</text>
<pin name="3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="DGND" urn="urn:adsk.eagle:component:16502426/6" prefix="SUPPLY" library_version="9">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt; - Digital Ground (DGND) Arrow</description>
<gates>
<gate name="G$1" symbol="DGND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name="">
<attribute name="CATEGORY" value="Supply" constant="no"/>
<attribute name="VALUE" value="DGND" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="5V0-CIRCLE" urn="urn:adsk.eagle:component:16502439/6" prefix="SUPPLY" library_version="9">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;  5 Volt (5V0) Circle</description>
<gates>
<gate name="G$1" symbol="5V0-CIRCLE" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name="">
<attribute name="CATEGORY" value="Supply" constant="no"/>
<attribute name="VALUE" value="5V0" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="3V3-CIRCLE" urn="urn:adsk.eagle:component:16502438/6" prefix="SUPPLY" library_version="9">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;  3.3 Volt (3V3) Circle</description>
<gates>
<gate name="G$1" symbol="3V3-CIRCLE" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name="">
<attribute name="CATEGORY" value="Supply" constant="no"/>
<attribute name="VALUE" value="3V3" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SMD-PHOTOCOUPLER-5KV-LTV-816S_4P-4.6X6.5MM_">
<packages>
<package name="SMD4-2.54-4.6X6.5MM">
<wire x1="-2.3" y1="3.25" x2="2.3" y2="3.25" width="0.127" layer="21"/>
<wire x1="2.3" y1="3.25" x2="2.3" y2="-3.25" width="0.127" layer="21"/>
<wire x1="2.3" y1="-3.25" x2="-2.3" y2="-3.25" width="0.127" layer="21"/>
<wire x1="-2.3" y1="-3.25" x2="-2.3" y2="3.25" width="0.127" layer="21"/>
<polygon width="0" layer="51">
<vertex x="-1.87" y="5.08"/>
<vertex x="-0.67" y="5.08"/>
<vertex x="-0.67" y="3.175"/>
<vertex x="-1.87" y="3.175"/>
</polygon>
<polygon width="0" layer="51">
<vertex x="0.67" y="5.08"/>
<vertex x="1.87" y="5.08"/>
<vertex x="1.87" y="3.175"/>
<vertex x="0.67" y="3.175"/>
</polygon>
<polygon width="0" layer="51">
<vertex x="-1.87" y="-3.3"/>
<vertex x="-0.67" y="-3.3"/>
<vertex x="-0.67" y="-5.08"/>
<vertex x="-1.87" y="-5.08"/>
</polygon>
<polygon width="0" layer="51">
<vertex x="0.67" y="-3.3"/>
<vertex x="1.87" y="-3.3"/>
<vertex x="1.87" y="-5.08"/>
<vertex x="0.67" y="-5.08"/>
</polygon>
<circle x="-1.27" y="-2.54" radius="0.635" width="0" layer="21"/>
<rectangle x1="-2.541390625" y1="-5.08276875" x2="2.54" y2="5.08" layer="39"/>
<text x="-2.542540625" y="-2.542540625" size="0.8136125" layer="25" rot="R90">&gt;NAME</text>
<text x="1.270809375" y="-2.54163125" size="0.81331875" layer="27" rot="R90">&gt;VALUE</text>
<smd name="1" x="-1.27" y="-4.68" dx="1.6" dy="1.4" layer="1" rot="R90"/>
<smd name="2" x="1.27" y="-4.68" dx="1.6" dy="1.4" layer="1" rot="R90"/>
<smd name="3" x="1.27" y="4.68" dx="1.6" dy="1.4" layer="1" rot="R90"/>
<smd name="4" x="-1.27" y="4.68" dx="1.6" dy="1.4" layer="1" rot="R90"/>
</package>
</packages>
<symbols>
<symbol name="PHOTOCOUPLER-LTV-816S">
<wire x1="-7.62" y1="7.62" x2="-5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="7.62" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="7.62" x2="7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="-7.62" x2="-5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-7.62" x2="-7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-7.62" x2="-7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-7.62" x2="-5.08" y2="-3.81" width="0.254" layer="94"/>
<polygon width="0" layer="94">
<vertex x="-1.27" y="-5.08"/>
<vertex x="-1.27" y="-2.54"/>
<vertex x="2.54" y="-3.81"/>
</polygon>
<wire x1="-5.08" y1="-3.81" x2="-1.27" y2="-3.81" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="-3.81" width="0.254" layer="94"/>
<wire x1="2.54" y1="-3.81" x2="2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="-3.81" x2="5.08" y2="-3.81" width="0.254" layer="94"/>
<wire x1="5.08" y1="-3.81" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="-5.08" y2="3.81" width="0.254" layer="94"/>
<wire x1="-5.08" y1="3.81" x2="-2.54" y2="3.81" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="3.81" x2="5.08" y2="3.81" width="0.254" layer="94"/>
<wire x1="5.08" y1="3.81" x2="5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="3.81" x2="1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="3.81" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="3.81" x2="1.905" y2="3.81" width="0.127" layer="94"/>
<wire x1="1.905" y1="3.81" x2="2.54" y2="3.175" width="0.127" layer="94"/>
<wire x1="2.54" y1="3.175" x2="2.54" y2="3.81" width="0.127" layer="94"/>
<text x="-10.1782" y="-2.54455" size="1.27228125" layer="95" rot="R90">&gt;NAME</text>
<text x="-6.36195" y="0" size="1.272390625" layer="96">&gt;VALUE</text>
<pin name="COLLECTOR" x="-5.08" y="10.16" visible="pad" length="short" rot="R270"/>
<pin name="EMITER" x="5.08" y="10.16" visible="pad" length="short" rot="R270"/>
<pin name="ANODE" x="-5.08" y="-10.16" visible="pad" length="short" rot="R90"/>
<pin name="CATHODE" x="5.08" y="-10.16" visible="pad" length="short" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SMD-PHOTOCOUPLER-5KV-LTV-816S(4P-4.6X6.5MM)" prefix="U">
<description>306060007 &lt;a href="https://pricing.snapeda.com/parts/LTV-816S/Lite-On%20Inc./view-part?ref=eda"&gt;Check availability&lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="PHOTOCOUPLER-LTV-816S" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SMD4-2.54-4.6X6.5MM">
<connects>
<connect gate="G$1" pin="ANODE" pad="1"/>
<connect gate="G$1" pin="CATHODE" pad="2"/>
<connect gate="G$1" pin="COLLECTOR" pad="4"/>
<connect gate="G$1" pin="EMITER" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="In Stock"/>
<attribute name="DESCRIPTION" value=" Optoisolator Transistor Output 5000Vrms 1 Channel 4-SMD "/>
<attribute name="MF" value="Lite-On Inc."/>
<attribute name="MP" value="LTV-816S"/>
<attribute name="PACKAGE" value="SMD-4 Lite-On Electronics"/>
<attribute name="PRICE" value="None"/>
<attribute name="PURCHASE-URL" value="https://pricing.snapeda.com/search/part/LTV-816S/?ref=eda"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Hardware" urn="urn:adsk.eagle:library:519">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
This library contains board components that are not electrical in nature, such as stand-offs, magnets, and Actobotics. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="STAND-OFF" urn="urn:adsk.eagle:footprint:38612/1" library_version="1">
<description>&lt;h3&gt;Standoff (#4 Screw)&lt;/h3&gt;
&lt;p&gt;This is the mechanical footprint for a #4 phillips button head screw. Use the keepout ring to avoid running the screw head into surrounding components.&lt;/p&gt;
&lt;h4&gt;Devices Using&lt;/h4&gt;
&lt;ul&gt;&lt;li&gt;STAND-OFF&lt;/li&gt;&lt;/ul&gt;</description>
<wire x1="0" y1="1.8542" x2="0" y2="-1.8542" width="0.2032" layer="41" curve="-180"/>
<wire x1="0" y1="-1.8542" x2="0" y2="1.8542" width="0.2032" layer="41" curve="-180"/>
<wire x1="0" y1="-1.8542" x2="0" y2="1.8542" width="0.2032" layer="42" curve="180"/>
<wire x1="0" y1="-1.8542" x2="0" y2="1.8542" width="0.2032" layer="42" curve="-180"/>
<circle x="0" y="0" radius="2.794" width="0.127" layer="39"/>
<hole x="0" y="0" drill="3.302"/>
<text x="0" y="2.032" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
</package>
<package name="STAND-OFF-TIGHT" urn="urn:adsk.eagle:footprint:38613/1" library_version="1">
<description>&lt;h3&gt;Standoff (#4 Screw) - Tight fit around screw body&lt;/h3&gt;
&lt;p&gt;This is the mechanical footprint for a #4 phillips button head screw. Use the keepout ring to avoid running the screw head into surrounding components.&lt;/p&gt;
&lt;h4&gt;Devices Using&lt;/h4&gt;
&lt;ul&gt;&lt;li&gt;STAND-OFF&lt;/li&gt;&lt;/ul&gt;</description>
<wire x1="0" y1="1.8542" x2="0" y2="-1.8542" width="0.2032" layer="41" curve="-180"/>
<wire x1="0" y1="-1.8542" x2="0" y2="1.8542" width="0.2032" layer="41" curve="-180"/>
<wire x1="0" y1="-1.8542" x2="0" y2="1.8542" width="0.2032" layer="42" curve="180"/>
<wire x1="0" y1="-1.8542" x2="0" y2="1.8542" width="0.2032" layer="42" curve="-180"/>
<circle x="0" y="0" radius="2.794" width="0.127" layer="39"/>
<hole x="0" y="0" drill="3.048"/>
<text x="0" y="1.651" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.651" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="STAND-OFF" urn="urn:adsk.eagle:package:38630/1" type="box" library_version="1">
<description>Standoff (#4 Screw)
This is the mechanical footprint for a #4 phillips button head screw. Use the keepout ring to avoid running the screw head into surrounding components.
Devices Using
STAND-OFF</description>
<packageinstances>
<packageinstance name="STAND-OFF"/>
</packageinstances>
</package3d>
<package3d name="STAND-OFF-TIGHT" urn="urn:adsk.eagle:package:38629/1" type="box" library_version="1">
<description>Standoff (#4 Screw) - Tight fit around screw body
This is the mechanical footprint for a #4 phillips button head screw. Use the keepout ring to avoid running the screw head into surrounding components.
Devices Using
STAND-OFF</description>
<packageinstances>
<packageinstance name="STAND-OFF-TIGHT"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="STAND-OFF" urn="urn:adsk.eagle:symbol:38611/1" library_version="1">
<description>&lt;h3&gt;Stand-Off Drill Hole&lt;/h3&gt;</description>
<circle x="0" y="0" radius="1.27" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="STAND-OFF" urn="urn:adsk.eagle:component:38644/1" prefix="H" library_version="1">
<description>&lt;h3&gt;Stand Off&lt;/h3&gt;
&lt;p&gt;Drill holes for mechanically mounting via screws, standoffs, etc.&lt;/p&gt;
&lt;p&gt;Note: Use the keepout ring to avoid running the screw head into surrounding components.
&lt;h4&gt;Variant Overviews&lt;/h4&gt;
&lt;ul&gt;
&lt;li&gt;&lt;b&gt;STAND-OFF&lt;/b&gt; - Mechanical footprint for a &lt;b&gt;#4 phillips button head&lt;/b&gt; screw.&lt;/li&gt;
&lt;li&gt;&lt;b&gt;STAND-OFF-TIGHT&lt;/b&gt; - Mechanical footprint for a &lt;b&gt;#4 phillips button head&lt;/b&gt; screw, &lt;/li&gt;
&lt;/ul&gt;
&lt;h4&gt;Example SparkFun Products&lt;/h4&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/10453"&gt;Screw - Phillips Head (1/4", 4-40, 10 pack)&lt;/a&gt; (PRT-10453)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/10452"&gt;Screw - Phillips Head (1/2", 4-40, 10 pack)&lt;/a&gt; (PRT-10452)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/10451"&gt;Screw - Phillips Head (3/4", 4-40, 10 pack)&lt;/a&gt; (PRT-10451)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/10450"&gt;Screw - Phillips Head (1", 4-40, 10 pack)&lt;/a&gt; (PRT-10450)&lt;/li&gt;</description>
<gates>
<gate name="G$1" symbol="STAND-OFF" x="0" y="0"/>
</gates>
<devices>
<device name="" package="STAND-OFF">
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38630/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TIGHT" package="STAND-OFF-TIGHT">
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38629/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Connectors" urn="urn:adsk.eagle:library:513">
<description>&lt;h3&gt;SparkFun Connectors&lt;/h3&gt;
This library contains electrically-functional connectors. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="2X20_SHROUDED" urn="urn:adsk.eagle:footprint:37952/1" library_version="1">
<description>&lt;h3&gt;Plated Through Hole - 2x20 Shrouded Header&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:40&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://cdn.sparkfun.com/datasheets/Dev/RaspberryPi/B-D-xx1X.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_20x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2.775" y1="24.765" x2="-2.775" y2="23.495" width="0.2032" layer="21"/>
<wire x1="4.5" y1="29.15" x2="4.5" y2="-29.15" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="-29.15" x2="-4.5" y2="29.15" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="29.15" x2="4.4" y2="29.15" width="0.2032" layer="21"/>
<wire x1="4.5" y1="-29.15" x2="-4.5" y2="-29.15" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="28.05" x2="3.4" y2="28.05" width="0.2032" layer="51"/>
<wire x1="3.4" y1="28.05" x2="3.4" y2="-28.05" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-28.05" x2="3.4" y2="-28.05" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="28.05" x2="-3.4" y2="2.2" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-28.05" x2="-3.4" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="2.2" x2="-3" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="-2.2" x2="-4.5" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="2.2" x2="-3" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-2.813" y1="24.765" x2="-2.813" y2="23.495" width="0.2032" layer="22"/>
<pad name="1" x="-1.27" y="24.13" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="2" x="1.27" y="24.13" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="3" x="-1.27" y="21.59" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="4" x="1.27" y="21.59" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="5" x="-1.27" y="19.05" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="6" x="1.27" y="19.05" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="7" x="-1.27" y="16.51" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="8" x="1.27" y="16.51" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="9" x="-1.27" y="13.97" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="10" x="1.27" y="13.97" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="11" x="-1.27" y="11.43" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="12" x="1.27" y="11.43" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="13" x="-1.27" y="8.89" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="14" x="1.27" y="8.89" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="15" x="-1.27" y="6.35" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="16" x="1.27" y="6.35" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="17" x="-1.27" y="3.81" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="18" x="1.27" y="3.81" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="19" x="-1.27" y="1.27" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="20" x="1.27" y="1.27" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="21" x="-1.27" y="-1.27" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="22" x="1.27" y="-1.27" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="23" x="-1.27" y="-3.81" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="24" x="1.27" y="-3.81" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="25" x="-1.27" y="-6.35" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="26" x="1.27" y="-6.35" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="27" x="-1.27" y="-8.89" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="28" x="1.27" y="-8.89" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="29" x="-1.27" y="-11.43" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="30" x="1.27" y="-11.43" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="31" x="-1.27" y="-13.97" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="32" x="1.27" y="-13.97" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="33" x="-1.27" y="-16.51" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="34" x="1.27" y="-16.51" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="35" x="-1.27" y="-19.05" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="36" x="1.27" y="-19.05" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="37" x="-1.27" y="-21.59" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="38" x="1.27" y="-21.59" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="39" x="-1.27" y="-24.13" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="40" x="1.27" y="-24.13" drill="1.016" diameter="1.8796" rot="R270"/>
<rectangle x1="-1.524" y1="23.876" x2="-1.016" y2="24.384" layer="51"/>
<rectangle x1="1.016" y1="23.876" x2="1.524" y2="24.384" layer="51"/>
<rectangle x1="1.016" y1="21.336" x2="1.524" y2="21.844" layer="51"/>
<rectangle x1="-1.524" y1="21.336" x2="-1.016" y2="21.844" layer="51"/>
<rectangle x1="1.016" y1="18.796" x2="1.524" y2="19.304" layer="51"/>
<rectangle x1="-1.524" y1="18.796" x2="-1.016" y2="19.304" layer="51"/>
<rectangle x1="1.016" y1="16.256" x2="1.524" y2="16.764" layer="51"/>
<rectangle x1="-1.524" y1="16.256" x2="-1.016" y2="16.764" layer="51"/>
<rectangle x1="1.016" y1="13.716" x2="1.524" y2="14.224" layer="51"/>
<rectangle x1="-1.524" y1="13.716" x2="-1.016" y2="14.224" layer="51"/>
<rectangle x1="-1.524" y1="16.256" x2="-1.016" y2="16.764" layer="51"/>
<rectangle x1="1.016" y1="16.256" x2="1.524" y2="16.764" layer="51"/>
<rectangle x1="-1.524" y1="11.176" x2="-1.016" y2="11.684" layer="51"/>
<rectangle x1="1.016" y1="11.176" x2="1.524" y2="11.684" layer="51"/>
<rectangle x1="1.016" y1="8.636" x2="1.524" y2="9.144" layer="51"/>
<rectangle x1="-1.524" y1="8.636" x2="-1.016" y2="9.144" layer="51"/>
<rectangle x1="1.016" y1="6.096" x2="1.524" y2="6.604" layer="51"/>
<rectangle x1="-1.524" y1="6.096" x2="-1.016" y2="6.604" layer="51"/>
<rectangle x1="1.016" y1="3.556" x2="1.524" y2="4.064" layer="51"/>
<rectangle x1="-1.524" y1="3.556" x2="-1.016" y2="4.064" layer="51"/>
<rectangle x1="1.016" y1="1.016" x2="1.524" y2="1.524" layer="51"/>
<rectangle x1="-1.524" y1="1.016" x2="-1.016" y2="1.524" layer="51"/>
<rectangle x1="-1.524" y1="3.556" x2="-1.016" y2="4.064" layer="51"/>
<rectangle x1="1.016" y1="3.556" x2="1.524" y2="4.064" layer="51"/>
<rectangle x1="1.016" y1="-1.524" x2="1.524" y2="-1.016" layer="51"/>
<rectangle x1="-1.524" y1="-1.524" x2="-1.016" y2="-1.016" layer="51"/>
<rectangle x1="1.016" y1="-4.064" x2="1.524" y2="-3.556" layer="51"/>
<rectangle x1="-1.524" y1="-4.064" x2="-1.016" y2="-3.556" layer="51"/>
<rectangle x1="1.016" y1="-6.604" x2="1.524" y2="-6.096" layer="51"/>
<rectangle x1="-1.524" y1="-6.604" x2="-1.016" y2="-6.096" layer="51"/>
<rectangle x1="-1.524" y1="-4.064" x2="-1.016" y2="-3.556" layer="51"/>
<rectangle x1="1.016" y1="-4.064" x2="1.524" y2="-3.556" layer="51"/>
<rectangle x1="1.016" y1="-9.144" x2="1.524" y2="-8.636" layer="51"/>
<rectangle x1="1.016" y1="-11.684" x2="1.524" y2="-11.176" layer="51"/>
<rectangle x1="1.016" y1="-14.224" x2="1.524" y2="-13.716" layer="51"/>
<rectangle x1="1.016" y1="-16.764" x2="1.524" y2="-16.256" layer="51"/>
<rectangle x1="1.016" y1="-19.304" x2="1.524" y2="-18.796" layer="51"/>
<rectangle x1="1.016" y1="-21.844" x2="1.524" y2="-21.336" layer="51"/>
<rectangle x1="1.016" y1="-24.384" x2="1.524" y2="-23.876" layer="51"/>
<rectangle x1="-1.524" y1="-9.144" x2="-1.016" y2="-8.636" layer="51"/>
<rectangle x1="-1.524" y1="-11.684" x2="-1.016" y2="-11.176" layer="51"/>
<rectangle x1="-1.524" y1="-14.224" x2="-1.016" y2="-13.716" layer="51"/>
<rectangle x1="-1.524" y1="-16.764" x2="-1.016" y2="-16.256" layer="51"/>
<rectangle x1="-1.524" y1="-19.304" x2="-1.016" y2="-18.796" layer="51"/>
<rectangle x1="-1.524" y1="-21.844" x2="-1.016" y2="-21.336" layer="51"/>
<rectangle x1="-1.524" y1="-24.384" x2="-1.016" y2="-23.876" layer="51"/>
<text x="-4.445" y="29.464" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-4.318" y="-30.099" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="2X20_SHROUDED_SMT" urn="urn:adsk.eagle:footprint:37953/1" library_version="1">
<description>&lt;h3&gt;Surface Mount - 2x20 Shrouded Header&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:40&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”http://sullinscorp.com/catalogs/145_PAGE118_.100_SBH11_SERIES_MALE_BOX_HDR_ST_RA_SMT.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_20x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-5.95" y1="24.765" x2="-5.95" y2="23.495" width="0.2032" layer="21"/>
<wire x1="4.5" y1="29.15" x2="4.5" y2="-29.15" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="-29.15" x2="-4.5" y2="29.15" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="29.15" x2="4.4" y2="29.15" width="0.2032" layer="21"/>
<wire x1="4.5" y1="-29.15" x2="-4.5" y2="-29.15" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="28.05" x2="3.4" y2="28.05" width="0.2032" layer="51"/>
<wire x1="3.4" y1="28.05" x2="3.4" y2="-28.05" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-28.05" x2="3.4" y2="-28.05" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="28.05" x2="-3.4" y2="2.2" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-28.05" x2="-3.4" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="2.2" x2="-3" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="-2.2" x2="-4.5" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="2.2" x2="-3" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-5.988" y1="24.765" x2="-5.988" y2="23.495" width="0.2032" layer="22"/>
<smd name="1" x="-3.5687" y="24.13" dx="4.1402" dy="0.9906" layer="1"/>
<smd name="2" x="3.5687" y="24.13" dx="4.1402" dy="0.9906" layer="1"/>
<smd name="3" x="-3.5687" y="21.59" dx="4.1402" dy="0.9906" layer="1"/>
<smd name="4" x="3.5687" y="21.59" dx="4.1402" dy="0.9906" layer="1"/>
<smd name="5" x="-3.5687" y="19.05" dx="4.1402" dy="0.9906" layer="1"/>
<smd name="6" x="3.5687" y="19.05" dx="4.1402" dy="0.9906" layer="1"/>
<smd name="7" x="-3.5687" y="16.51" dx="4.1402" dy="0.9906" layer="1"/>
<smd name="8" x="3.5687" y="16.51" dx="4.1402" dy="0.9906" layer="1"/>
<smd name="9" x="-3.5687" y="13.97" dx="4.1402" dy="0.9906" layer="1"/>
<smd name="10" x="3.5687" y="13.97" dx="4.1402" dy="0.9906" layer="1"/>
<smd name="11" x="-3.5687" y="11.43" dx="4.1402" dy="0.9906" layer="1"/>
<smd name="12" x="3.5687" y="11.43" dx="4.1402" dy="0.9906" layer="1"/>
<smd name="13" x="-3.5687" y="8.89" dx="4.1402" dy="0.9906" layer="1"/>
<smd name="14" x="3.5687" y="8.89" dx="4.1402" dy="0.9906" layer="1"/>
<smd name="15" x="-3.5687" y="6.35" dx="4.1402" dy="0.9906" layer="1"/>
<smd name="16" x="3.5687" y="6.35" dx="4.1402" dy="0.9906" layer="1"/>
<smd name="17" x="-3.5687" y="3.81" dx="4.1402" dy="0.9906" layer="1"/>
<smd name="18" x="3.5687" y="3.81" dx="4.1402" dy="0.9906" layer="1"/>
<smd name="19" x="-3.5687" y="1.27" dx="4.1402" dy="0.9906" layer="1"/>
<smd name="20" x="3.5687" y="1.27" dx="4.1402" dy="0.9906" layer="1"/>
<smd name="21" x="-3.5687" y="-1.27" dx="4.1402" dy="0.9906" layer="1"/>
<smd name="22" x="3.5687" y="-1.27" dx="4.1402" dy="0.9906" layer="1"/>
<smd name="23" x="-3.5687" y="-3.81" dx="4.1402" dy="0.9906" layer="1"/>
<smd name="24" x="3.5687" y="-3.81" dx="4.1402" dy="0.9906" layer="1"/>
<smd name="25" x="-3.5687" y="-6.35" dx="4.1402" dy="0.9906" layer="1"/>
<smd name="26" x="3.5687" y="-6.35" dx="4.1402" dy="0.9906" layer="1"/>
<smd name="27" x="-3.5687" y="-8.89" dx="4.1402" dy="0.9906" layer="1"/>
<smd name="28" x="3.5687" y="-8.89" dx="4.1402" dy="0.9906" layer="1"/>
<smd name="29" x="-3.5687" y="-11.43" dx="4.1402" dy="0.9906" layer="1"/>
<smd name="30" x="3.5687" y="-11.43" dx="4.1402" dy="0.9906" layer="1"/>
<smd name="31" x="-3.5687" y="-13.97" dx="4.1402" dy="0.9906" layer="1"/>
<smd name="32" x="3.5687" y="-13.97" dx="4.1402" dy="0.9906" layer="1"/>
<smd name="33" x="-3.5687" y="-16.51" dx="4.1402" dy="0.9906" layer="1"/>
<smd name="34" x="3.5687" y="-16.51" dx="4.1402" dy="0.9906" layer="1"/>
<smd name="35" x="-3.5687" y="-19.05" dx="4.1402" dy="0.9906" layer="1"/>
<smd name="36" x="3.5687" y="-19.05" dx="4.1402" dy="0.9906" layer="1"/>
<smd name="37" x="-3.5687" y="-21.59" dx="4.1402" dy="0.9906" layer="1"/>
<smd name="38" x="3.5687" y="-21.59" dx="4.1402" dy="0.9906" layer="1"/>
<smd name="39" x="-3.5687" y="-24.13" dx="4.1402" dy="0.9906" layer="1"/>
<smd name="40" x="3.5687" y="-24.13" dx="4.1402" dy="0.9906" layer="1"/>
<text x="-4.445" y="29.337" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-4.318" y="-30.099" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="2X20" urn="urn:adsk.eagle:footprint:37950/1" library_version="1">
<description>&lt;h3&gt;Plated Through Hole - 2x20&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:40&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_20x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="16.51" y1="-0.635" x2="15.875" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="13.335" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="-1.27" x2="12.065" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="-1.27" x2="11.43" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="14.605" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="15.875" y1="-1.27" x2="14.605" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-0.635" x2="10.795" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="-1.27" x2="9.525" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="31.75" y1="-0.635" x2="31.115" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="31.115" y1="-1.27" x2="29.845" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="29.845" y1="-1.27" x2="29.21" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="26.67" y1="-0.635" x2="26.035" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="29.21" y1="-0.635" x2="28.575" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="28.575" y1="-1.27" x2="27.305" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="27.305" y1="-1.27" x2="26.67" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="24.13" y1="-0.635" x2="23.495" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="23.495" y1="-1.27" x2="22.225" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="22.225" y1="-1.27" x2="21.59" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="24.13" y1="-0.635" x2="24.765" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="26.035" y1="-1.27" x2="24.765" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="19.05" y1="-0.635" x2="18.415" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="21.59" y1="-0.635" x2="20.955" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="20.955" y1="-1.27" x2="19.685" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="19.685" y1="-1.27" x2="19.05" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="16.51" y1="-0.635" x2="17.145" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="18.415" y1="-1.27" x2="17.145" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="14.605" y1="3.81" x2="15.875" y2="3.81" width="0.2032" layer="21"/>
<wire x1="15.875" y1="3.81" x2="16.51" y2="3.175" width="0.2032" layer="21"/>
<wire x1="11.43" y1="3.175" x2="12.065" y2="3.81" width="0.2032" layer="21"/>
<wire x1="12.065" y1="3.81" x2="13.335" y2="3.81" width="0.2032" layer="21"/>
<wire x1="13.335" y1="3.81" x2="13.97" y2="3.175" width="0.2032" layer="21"/>
<wire x1="14.605" y1="3.81" x2="13.97" y2="3.175" width="0.2032" layer="21"/>
<wire x1="6.985" y1="3.81" x2="8.255" y2="3.81" width="0.2032" layer="21"/>
<wire x1="8.255" y1="3.81" x2="8.89" y2="3.175" width="0.2032" layer="21"/>
<wire x1="8.89" y1="3.175" x2="9.525" y2="3.81" width="0.2032" layer="21"/>
<wire x1="9.525" y1="3.81" x2="10.795" y2="3.81" width="0.2032" layer="21"/>
<wire x1="10.795" y1="3.81" x2="11.43" y2="3.175" width="0.2032" layer="21"/>
<wire x1="3.81" y1="3.175" x2="4.445" y2="3.81" width="0.2032" layer="21"/>
<wire x1="4.445" y1="3.81" x2="5.715" y2="3.81" width="0.2032" layer="21"/>
<wire x1="5.715" y1="3.81" x2="6.35" y2="3.175" width="0.2032" layer="21"/>
<wire x1="6.985" y1="3.81" x2="6.35" y2="3.175" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="3.81" x2="0.635" y2="3.81" width="0.2032" layer="21"/>
<wire x1="0.635" y1="3.81" x2="1.27" y2="3.175" width="0.2032" layer="21"/>
<wire x1="1.27" y1="3.175" x2="1.905" y2="3.81" width="0.2032" layer="21"/>
<wire x1="1.905" y1="3.81" x2="3.175" y2="3.81" width="0.2032" layer="21"/>
<wire x1="3.175" y1="3.81" x2="3.81" y2="3.175" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="3.175" x2="-1.27" y2="1.905" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="29.21" y1="3.175" x2="29.845" y2="3.81" width="0.2032" layer="21"/>
<wire x1="29.845" y1="3.81" x2="31.115" y2="3.81" width="0.2032" layer="21"/>
<wire x1="31.115" y1="3.81" x2="31.75" y2="3.175" width="0.2032" layer="21"/>
<wire x1="24.765" y1="3.81" x2="26.035" y2="3.81" width="0.2032" layer="21"/>
<wire x1="26.035" y1="3.81" x2="26.67" y2="3.175" width="0.2032" layer="21"/>
<wire x1="26.67" y1="3.175" x2="27.305" y2="3.81" width="0.2032" layer="21"/>
<wire x1="27.305" y1="3.81" x2="28.575" y2="3.81" width="0.2032" layer="21"/>
<wire x1="28.575" y1="3.81" x2="29.21" y2="3.175" width="0.2032" layer="21"/>
<wire x1="21.59" y1="3.175" x2="22.225" y2="3.81" width="0.2032" layer="21"/>
<wire x1="22.225" y1="3.81" x2="23.495" y2="3.81" width="0.2032" layer="21"/>
<wire x1="23.495" y1="3.81" x2="24.13" y2="3.175" width="0.2032" layer="21"/>
<wire x1="24.765" y1="3.81" x2="24.13" y2="3.175" width="0.2032" layer="21"/>
<wire x1="17.145" y1="3.81" x2="18.415" y2="3.81" width="0.2032" layer="21"/>
<wire x1="18.415" y1="3.81" x2="19.05" y2="3.175" width="0.2032" layer="21"/>
<wire x1="19.05" y1="3.175" x2="19.685" y2="3.81" width="0.2032" layer="21"/>
<wire x1="19.685" y1="3.81" x2="20.955" y2="3.81" width="0.2032" layer="21"/>
<wire x1="20.955" y1="3.81" x2="21.59" y2="3.175" width="0.2032" layer="21"/>
<wire x1="17.145" y1="3.81" x2="16.51" y2="3.175" width="0.2032" layer="21"/>
<wire x1="1.27" y1="3.175" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="3.175" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="3.175" x2="6.35" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="3.175" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="11.43" y1="3.175" x2="11.43" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="3.175" x2="13.97" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="16.51" y1="3.175" x2="16.51" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="19.05" y1="3.175" x2="19.05" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="21.59" y1="3.175" x2="21.59" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="24.13" y1="3.175" x2="24.13" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="26.67" y1="3.175" x2="26.67" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="29.21" y1="3.175" x2="29.21" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="49.53" y1="1.905" x2="49.53" y2="3.175" width="0.2032" layer="21"/>
<wire x1="49.53" y1="-0.635" x2="49.53" y2="0.635" width="0.2032" layer="21"/>
<wire x1="48.895" y1="1.27" x2="49.53" y2="0.635" width="0.2032" layer="21"/>
<wire x1="49.53" y1="1.905" x2="48.895" y2="1.27" width="0.2032" layer="21"/>
<wire x1="32.385" y1="3.81" x2="33.655" y2="3.81" width="0.2032" layer="21"/>
<wire x1="33.655" y1="3.81" x2="34.29" y2="3.175" width="0.2032" layer="21"/>
<wire x1="32.385" y1="3.81" x2="31.75" y2="3.175" width="0.2032" layer="21"/>
<wire x1="46.99" y1="3.175" x2="47.625" y2="3.81" width="0.2032" layer="21"/>
<wire x1="47.625" y1="3.81" x2="48.895" y2="3.81" width="0.2032" layer="21"/>
<wire x1="48.895" y1="3.81" x2="49.53" y2="3.175" width="0.2032" layer="21"/>
<wire x1="42.545" y1="3.81" x2="43.815" y2="3.81" width="0.2032" layer="21"/>
<wire x1="43.815" y1="3.81" x2="44.45" y2="3.175" width="0.2032" layer="21"/>
<wire x1="44.45" y1="3.175" x2="45.085" y2="3.81" width="0.2032" layer="21"/>
<wire x1="45.085" y1="3.81" x2="46.355" y2="3.81" width="0.2032" layer="21"/>
<wire x1="46.355" y1="3.81" x2="46.99" y2="3.175" width="0.2032" layer="21"/>
<wire x1="39.37" y1="3.175" x2="40.005" y2="3.81" width="0.2032" layer="21"/>
<wire x1="40.005" y1="3.81" x2="41.275" y2="3.81" width="0.2032" layer="21"/>
<wire x1="41.275" y1="3.81" x2="41.91" y2="3.175" width="0.2032" layer="21"/>
<wire x1="42.545" y1="3.81" x2="41.91" y2="3.175" width="0.2032" layer="21"/>
<wire x1="34.925" y1="3.81" x2="36.195" y2="3.81" width="0.2032" layer="21"/>
<wire x1="36.195" y1="3.81" x2="36.83" y2="3.175" width="0.2032" layer="21"/>
<wire x1="36.83" y1="3.175" x2="37.465" y2="3.81" width="0.2032" layer="21"/>
<wire x1="37.465" y1="3.81" x2="38.735" y2="3.81" width="0.2032" layer="21"/>
<wire x1="38.735" y1="3.81" x2="39.37" y2="3.175" width="0.2032" layer="21"/>
<wire x1="34.925" y1="3.81" x2="34.29" y2="3.175" width="0.2032" layer="21"/>
<wire x1="31.75" y1="3.175" x2="31.75" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="34.29" y1="3.175" x2="34.29" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="36.83" y1="3.175" x2="36.83" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="39.37" y1="3.175" x2="39.37" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="41.91" y1="3.175" x2="41.91" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="44.45" y1="3.175" x2="44.45" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="46.99" y1="3.175" x2="46.99" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="48.895" y1="-1.27" x2="47.625" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="47.625" y1="-1.27" x2="46.99" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="48.895" y1="-1.27" x2="49.53" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="34.29" y1="-0.635" x2="33.655" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="33.655" y1="-1.27" x2="32.385" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="32.385" y1="-1.27" x2="31.75" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="38.735" y1="-1.27" x2="37.465" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="37.465" y1="-1.27" x2="36.83" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="36.83" y1="-0.635" x2="36.195" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="36.195" y1="-1.27" x2="34.925" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="34.925" y1="-1.27" x2="34.29" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="41.91" y1="-0.635" x2="41.275" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="41.275" y1="-1.27" x2="40.005" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="40.005" y1="-1.27" x2="39.37" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="38.735" y1="-1.27" x2="39.37" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="46.355" y1="-1.27" x2="45.085" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-1.27" x2="44.45" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-0.635" x2="43.815" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="43.815" y1="-1.27" x2="42.545" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-1.27" x2="41.91" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="46.355" y1="-1.27" x2="46.99" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="3.175" x2="-0.635" y2="3.81" width="0.2032" layer="21"/>
<wire x1="-0.889" y1="-1.651" x2="0.762" y2="-1.651" width="0.2032" layer="21"/>
<wire x1="-0.889" y1="-1.651" x2="0.762" y2="-1.651" width="0.2032" layer="22"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="5" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="7" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="9" x="10.16" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="11" x="12.7" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="13" x="15.24" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="15" x="17.78" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="17" x="20.32" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="19" x="22.86" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="21" x="25.4" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="23" x="27.94" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="25" x="30.48" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="26" x="30.48" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="24" x="27.94" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="22" x="25.4" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="20" x="22.86" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="18" x="20.32" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="16" x="17.78" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="14" x="15.24" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="12" x="12.7" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="10" x="10.16" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="8" x="7.62" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="6" x="5.08" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="4" x="2.54" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="2" x="0" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="27" x="33.02" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="28" x="33.02" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="29" x="35.56" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="30" x="35.56" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="31" x="38.1" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="32" x="38.1" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="33" x="40.64" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="34" x="40.64" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="35" x="43.18" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="36" x="43.18" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="37" x="45.72" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="38" x="45.72" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="39" x="48.26" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="40" x="48.26" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<rectangle x1="14.986" y1="-0.254" x2="15.494" y2="0.254" layer="51"/>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="30.226" y1="-0.254" x2="30.734" y2="0.254" layer="51"/>
<rectangle x1="27.686" y1="-0.254" x2="28.194" y2="0.254" layer="51"/>
<rectangle x1="25.146" y1="-0.254" x2="25.654" y2="0.254" layer="51"/>
<rectangle x1="22.606" y1="-0.254" x2="23.114" y2="0.254" layer="51"/>
<rectangle x1="20.066" y1="-0.254" x2="20.574" y2="0.254" layer="51"/>
<rectangle x1="17.526" y1="-0.254" x2="18.034" y2="0.254" layer="51"/>
<rectangle x1="14.986" y1="2.286" x2="15.494" y2="2.794" layer="51" rot="R180"/>
<rectangle x1="17.526" y1="2.286" x2="18.034" y2="2.794" layer="51" rot="R180"/>
<rectangle x1="20.066" y1="2.286" x2="20.574" y2="2.794" layer="51" rot="R180"/>
<rectangle x1="22.606" y1="2.286" x2="23.114" y2="2.794" layer="51" rot="R180"/>
<rectangle x1="25.146" y1="2.286" x2="25.654" y2="2.794" layer="51" rot="R180"/>
<rectangle x1="27.686" y1="2.286" x2="28.194" y2="2.794" layer="51" rot="R180"/>
<rectangle x1="30.226" y1="2.286" x2="30.734" y2="2.794" layer="51" rot="R180"/>
<rectangle x1="-0.254" y1="2.286" x2="0.254" y2="2.794" layer="51" rot="R180"/>
<rectangle x1="2.286" y1="2.286" x2="2.794" y2="2.794" layer="51" rot="R180"/>
<rectangle x1="4.826" y1="2.286" x2="5.334" y2="2.794" layer="51" rot="R180"/>
<rectangle x1="7.366" y1="2.286" x2="7.874" y2="2.794" layer="51" rot="R180"/>
<rectangle x1="9.906" y1="2.286" x2="10.414" y2="2.794" layer="51" rot="R180"/>
<rectangle x1="12.446" y1="2.286" x2="12.954" y2="2.794" layer="51" rot="R180"/>
<rectangle x1="32.766" y1="2.286" x2="33.274" y2="2.794" layer="51" rot="R180"/>
<rectangle x1="35.306" y1="2.286" x2="35.814" y2="2.794" layer="51" rot="R180"/>
<rectangle x1="37.846" y1="2.286" x2="38.354" y2="2.794" layer="51" rot="R180"/>
<rectangle x1="40.386" y1="2.286" x2="40.894" y2="2.794" layer="51" rot="R180"/>
<rectangle x1="42.926" y1="2.286" x2="43.434" y2="2.794" layer="51" rot="R180"/>
<rectangle x1="45.466" y1="2.286" x2="45.974" y2="2.794" layer="51" rot="R180"/>
<rectangle x1="48.006" y1="2.286" x2="48.514" y2="2.794" layer="51" rot="R180"/>
<rectangle x1="32.766" y1="-0.254" x2="33.274" y2="0.254" layer="51" rot="R180"/>
<rectangle x1="35.306" y1="-0.254" x2="35.814" y2="0.254" layer="51" rot="R180"/>
<rectangle x1="37.846" y1="-0.254" x2="38.354" y2="0.254" layer="51" rot="R180"/>
<rectangle x1="40.386" y1="-0.254" x2="40.894" y2="0.254" layer="51" rot="R180"/>
<rectangle x1="42.926" y1="-0.254" x2="43.434" y2="0.254" layer="51" rot="R180"/>
<rectangle x1="45.466" y1="-0.254" x2="45.974" y2="0.254" layer="51" rot="R180"/>
<rectangle x1="48.006" y1="-0.254" x2="48.514" y2="0.254" layer="51" rot="R180"/>
<text x="-1.27" y="4.064" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.413" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="2X20_SHROUDED" urn="urn:adsk.eagle:package:38279/1" type="box" library_version="1">
<description>Plated Through Hole - 2x20 Shrouded Header
Specifications:
Pin count:40
Pin pitch:0.1"

Datasheet referenced for footprint
Example device(s):
CONN_20x2
</description>
<packageinstances>
<packageinstance name="2X20_SHROUDED"/>
</packageinstances>
</package3d>
<package3d name="2X20_SHROUDED_SMT" urn="urn:adsk.eagle:package:38280/1" type="box" library_version="1">
<description>Surface Mount - 2x20 Shrouded Header
Specifications:
Pin count:40
Pin pitch:0.1"

Datasheet referenced for footprint
Example device(s):
CONN_20x2
</description>
<packageinstances>
<packageinstance name="2X20_SHROUDED_SMT"/>
</packageinstances>
</package3d>
<package3d name="2X20" urn="urn:adsk.eagle:package:38282/1" type="box" library_version="1">
<description>Plated Through Hole - 2x20
Specifications:
Pin count:40
Pin pitch:0.1"

Example device(s):
CONN_20x2
</description>
<packageinstances>
<packageinstance name="2X20"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="RASPBERRYPI_40_PIN_GPIO" urn="urn:adsk.eagle:symbol:37985/1" library_version="1">
<wire x1="-12.7" y1="27.94" x2="12.7" y2="27.94" width="0.254" layer="94"/>
<wire x1="12.7" y1="27.94" x2="12.7" y2="-33.02" width="0.254" layer="94"/>
<wire x1="12.7" y1="-33.02" x2="-12.7" y2="-33.02" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-33.02" x2="-12.7" y2="27.94" width="0.254" layer="94"/>
<wire x1="-3.81" y1="12.7" x2="-3.81" y2="-7.62" width="0.254" layer="95"/>
<wire x1="3.556" y1="-27.94" x2="3.556" y2="-30.48" width="0.254" layer="95"/>
<pin name="3.3V@1" x="-15.24" y="20.32" length="short" direction="pwr"/>
<pin name="SDA" x="15.24" y="25.4" length="short" rot="R180"/>
<pin name="SCL" x="15.24" y="22.86" length="short" rot="R180"/>
<pin name="GP4" x="15.24" y="-5.08" length="short" rot="R180"/>
<pin name="GND@9" x="-15.24" y="-15.24" length="short" direction="pwr"/>
<pin name="GP17" x="15.24" y="-7.62" length="short" rot="R180"/>
<pin name="GP27" x="15.24" y="-22.86" length="short" rot="R180"/>
<pin name="GP22" x="15.24" y="-12.7" length="short" rot="R180"/>
<pin name="3.3V@17" x="-15.24" y="17.78" length="short" direction="pwr"/>
<pin name="MOSI" x="15.24" y="5.08" length="short" direction="out" rot="R180"/>
<pin name="MISO" x="15.24" y="7.62" length="short" direction="in" rot="R180"/>
<pin name="SCLK" x="15.24" y="10.16" length="short" direction="out" rot="R180"/>
<pin name="GND@25" x="-15.24" y="-22.86" length="short" direction="pwr"/>
<pin name="CE1" x="15.24" y="0" length="short" direction="out" rot="R180"/>
<pin name="CE0" x="15.24" y="2.54" length="short" direction="out" rot="R180"/>
<pin name="GP25" x="15.24" y="-20.32" length="short" rot="R180"/>
<pin name="GND@20" x="-15.24" y="-20.32" length="short" direction="pwr"/>
<pin name="GP24" x="15.24" y="-17.78" length="short" rot="R180"/>
<pin name="GP23" x="15.24" y="-15.24" length="short" rot="R180"/>
<pin name="GND@14" x="-15.24" y="-17.78" length="short" direction="pwr"/>
<pin name="GP18#" x="15.24" y="-10.16" length="short" rot="R180"/>
<pin name="RXI" x="15.24" y="15.24" length="short" direction="in" rot="R180"/>
<pin name="TXO" x="15.24" y="17.78" length="short" direction="out" rot="R180"/>
<pin name="GND@6" x="-15.24" y="-12.7" length="short" direction="pwr"/>
<pin name="5V@4" x="-15.24" y="22.86" length="short" direction="pwr"/>
<pin name="5V@2" x="-15.24" y="25.4" length="short" direction="pwr"/>
<pin name="GND@30" x="-15.24" y="-25.4" length="short" direction="pwr"/>
<pin name="GND@34" x="-15.24" y="-27.94" length="short" direction="pwr"/>
<pin name="GND@39" x="-15.24" y="-30.48" length="short" direction="pwr"/>
<pin name="ID_SD" x="15.24" y="-27.94" length="short" rot="R180"/>
<pin name="ID_SC" x="15.24" y="-30.48" length="short" rot="R180"/>
<pin name="GP5" x="-15.24" y="12.7" length="short"/>
<pin name="GP6" x="-15.24" y="10.16" length="short"/>
<pin name="GP12" x="-15.24" y="7.62" length="short"/>
<pin name="GP13" x="-15.24" y="5.08" length="short"/>
<pin name="GP19" x="-15.24" y="0" length="short"/>
<pin name="GP16" x="-15.24" y="2.54" length="short"/>
<pin name="GP26" x="-15.24" y="-7.62" length="short"/>
<pin name="GP20" x="-15.24" y="-2.54" length="short"/>
<pin name="GP21" x="-15.24" y="-5.08" length="short"/>
<text x="-12.7" y="28.702" size="1.778" layer="95">&gt;Name</text>
<text x="-12.7" y="-35.56" size="1.778" layer="96">&gt;Value</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="RASPBERRYPI-40-PIN-GPIO" urn="urn:adsk.eagle:component:38387/1" prefix="J" library_version="1">
<description>&lt;h3&gt;Raspberry Pi GPIO Header&lt;/h3&gt;
&lt;p&gt;2x20 pin connector, as found on B, B+, A+ models. Both shrouded PTH and SMT versions available.&lt;/p&gt;
&lt;p&gt;&lt;b&gt;SparkFun Products:&lt;/b&gt;
&lt;ul&gt;&lt;li&gt;&lt;a href=”https://www.sparkfun.com/products/13054”&gt;GPIO Shrouded Header&lt;/a&gt;- PTH&lt;/li&gt;
&lt;li&gt;&lt;a href=”https://www.sparkfun.com/products/13717”&gt;SparkFun Pi Wedge (Preassembled)&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="RASPBERRYPI_40_PIN_GPIO" x="0" y="2.54"/>
</gates>
<devices>
<device name="_PTH" package="2X20_SHROUDED">
<connects>
<connect gate="G$1" pin="3.3V@1" pad="1"/>
<connect gate="G$1" pin="3.3V@17" pad="17"/>
<connect gate="G$1" pin="5V@2" pad="2"/>
<connect gate="G$1" pin="5V@4" pad="4"/>
<connect gate="G$1" pin="CE0" pad="24"/>
<connect gate="G$1" pin="CE1" pad="26"/>
<connect gate="G$1" pin="GND@14" pad="14"/>
<connect gate="G$1" pin="GND@20" pad="20"/>
<connect gate="G$1" pin="GND@25" pad="25"/>
<connect gate="G$1" pin="GND@30" pad="30"/>
<connect gate="G$1" pin="GND@34" pad="34"/>
<connect gate="G$1" pin="GND@39" pad="39"/>
<connect gate="G$1" pin="GND@6" pad="6"/>
<connect gate="G$1" pin="GND@9" pad="9"/>
<connect gate="G$1" pin="GP12" pad="32"/>
<connect gate="G$1" pin="GP13" pad="33"/>
<connect gate="G$1" pin="GP16" pad="36"/>
<connect gate="G$1" pin="GP17" pad="11"/>
<connect gate="G$1" pin="GP18#" pad="12"/>
<connect gate="G$1" pin="GP19" pad="35"/>
<connect gate="G$1" pin="GP20" pad="38"/>
<connect gate="G$1" pin="GP21" pad="40"/>
<connect gate="G$1" pin="GP22" pad="15"/>
<connect gate="G$1" pin="GP23" pad="16"/>
<connect gate="G$1" pin="GP24" pad="18"/>
<connect gate="G$1" pin="GP25" pad="22"/>
<connect gate="G$1" pin="GP26" pad="37"/>
<connect gate="G$1" pin="GP27" pad="13"/>
<connect gate="G$1" pin="GP4" pad="7"/>
<connect gate="G$1" pin="GP5" pad="29"/>
<connect gate="G$1" pin="GP6" pad="31"/>
<connect gate="G$1" pin="ID_SC" pad="28"/>
<connect gate="G$1" pin="ID_SD" pad="27"/>
<connect gate="G$1" pin="MISO" pad="21"/>
<connect gate="G$1" pin="MOSI" pad="19"/>
<connect gate="G$1" pin="RXI" pad="10"/>
<connect gate="G$1" pin="SCL" pad="5"/>
<connect gate="G$1" pin="SCLK" pad="23"/>
<connect gate="G$1" pin="SDA" pad="3"/>
<connect gate="G$1" pin="TXO" pad="8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38279/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-12263" constant="no"/>
<attribute name="SF_ID" value="PRT-13054" constant="no"/>
</technology>
</technologies>
</device>
<device name="_SMT" package="2X20_SHROUDED_SMT">
<connects>
<connect gate="G$1" pin="3.3V@1" pad="1"/>
<connect gate="G$1" pin="3.3V@17" pad="17"/>
<connect gate="G$1" pin="5V@2" pad="2"/>
<connect gate="G$1" pin="5V@4" pad="4"/>
<connect gate="G$1" pin="CE0" pad="24"/>
<connect gate="G$1" pin="CE1" pad="26"/>
<connect gate="G$1" pin="GND@14" pad="14"/>
<connect gate="G$1" pin="GND@20" pad="20"/>
<connect gate="G$1" pin="GND@25" pad="25"/>
<connect gate="G$1" pin="GND@30" pad="30"/>
<connect gate="G$1" pin="GND@34" pad="34"/>
<connect gate="G$1" pin="GND@39" pad="39"/>
<connect gate="G$1" pin="GND@6" pad="6"/>
<connect gate="G$1" pin="GND@9" pad="9"/>
<connect gate="G$1" pin="GP12" pad="32"/>
<connect gate="G$1" pin="GP13" pad="33"/>
<connect gate="G$1" pin="GP16" pad="36"/>
<connect gate="G$1" pin="GP17" pad="11"/>
<connect gate="G$1" pin="GP18#" pad="12"/>
<connect gate="G$1" pin="GP19" pad="35"/>
<connect gate="G$1" pin="GP20" pad="38"/>
<connect gate="G$1" pin="GP21" pad="40"/>
<connect gate="G$1" pin="GP22" pad="15"/>
<connect gate="G$1" pin="GP23" pad="16"/>
<connect gate="G$1" pin="GP24" pad="18"/>
<connect gate="G$1" pin="GP25" pad="22"/>
<connect gate="G$1" pin="GP26" pad="37"/>
<connect gate="G$1" pin="GP27" pad="13"/>
<connect gate="G$1" pin="GP4" pad="7"/>
<connect gate="G$1" pin="GP5" pad="29"/>
<connect gate="G$1" pin="GP6" pad="31"/>
<connect gate="G$1" pin="ID_SC" pad="28"/>
<connect gate="G$1" pin="ID_SD" pad="27"/>
<connect gate="G$1" pin="MISO" pad="21"/>
<connect gate="G$1" pin="MOSI" pad="19"/>
<connect gate="G$1" pin="RXI" pad="10"/>
<connect gate="G$1" pin="SCL" pad="5"/>
<connect gate="G$1" pin="SCLK" pad="23"/>
<connect gate="G$1" pin="SDA" pad="3"/>
<connect gate="G$1" pin="TXO" pad="8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38280/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-13143" constant="no"/>
</technology>
</technologies>
</device>
<device name="_PTH_NO_SHROUD" package="2X20">
<connects>
<connect gate="G$1" pin="3.3V@1" pad="1"/>
<connect gate="G$1" pin="3.3V@17" pad="17"/>
<connect gate="G$1" pin="5V@2" pad="2"/>
<connect gate="G$1" pin="5V@4" pad="4"/>
<connect gate="G$1" pin="CE0" pad="24"/>
<connect gate="G$1" pin="CE1" pad="26"/>
<connect gate="G$1" pin="GND@14" pad="14"/>
<connect gate="G$1" pin="GND@20" pad="20"/>
<connect gate="G$1" pin="GND@25" pad="25"/>
<connect gate="G$1" pin="GND@30" pad="30"/>
<connect gate="G$1" pin="GND@34" pad="34"/>
<connect gate="G$1" pin="GND@39" pad="39"/>
<connect gate="G$1" pin="GND@6" pad="6"/>
<connect gate="G$1" pin="GND@9" pad="9"/>
<connect gate="G$1" pin="GP12" pad="32"/>
<connect gate="G$1" pin="GP13" pad="33"/>
<connect gate="G$1" pin="GP16" pad="36"/>
<connect gate="G$1" pin="GP17" pad="11"/>
<connect gate="G$1" pin="GP18#" pad="12"/>
<connect gate="G$1" pin="GP19" pad="35"/>
<connect gate="G$1" pin="GP20" pad="38"/>
<connect gate="G$1" pin="GP21" pad="40"/>
<connect gate="G$1" pin="GP22" pad="15"/>
<connect gate="G$1" pin="GP23" pad="16"/>
<connect gate="G$1" pin="GP24" pad="18"/>
<connect gate="G$1" pin="GP25" pad="22"/>
<connect gate="G$1" pin="GP26" pad="37"/>
<connect gate="G$1" pin="GP27" pad="13"/>
<connect gate="G$1" pin="GP4" pad="7"/>
<connect gate="G$1" pin="GP5" pad="29"/>
<connect gate="G$1" pin="GP6" pad="31"/>
<connect gate="G$1" pin="ID_SC" pad="28"/>
<connect gate="G$1" pin="ID_SD" pad="27"/>
<connect gate="G$1" pin="MISO" pad="21"/>
<connect gate="G$1" pin="MOSI" pad="19"/>
<connect gate="G$1" pin="RXI" pad="10"/>
<connect gate="G$1" pin="SCL" pad="5"/>
<connect gate="G$1" pin="SCLK" pad="23"/>
<connect gate="G$1" pin="SDA" pad="3"/>
<connect gate="G$1" pin="TXO" pad="8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38282/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
<class number="1" name="gnd" width="0" drill="0">
<clearance class="1" value="0.6604"/>
</class>
</classes>
<parts>
<part name="U1" library="T2535-800G-TR" deviceset="T2535-800G-TR" device=""/>
<part name="CON1" library="con-wago-500" library_urn="urn:adsk.eagle:library:195" deviceset="W237-102" device="" package3d_urn="urn:adsk.eagle:package:10688/1" value="~240V"/>
<part name="U2" library="MOC3063M_" deviceset="MOC3063M*" device="S"/>
<part name="R1" library="SparkFun-Resistors" library_urn="urn:adsk.eagle:library:532" deviceset="RESISTOR" device="1206" package3d_urn="urn:adsk.eagle:package:39654/1" value="360R"/>
<part name="R2" library="SparkFun-Resistors" library_urn="urn:adsk.eagle:library:532" deviceset="RESISTOR" device="1206" package3d_urn="urn:adsk.eagle:package:39654/1" value="360R"/>
<part name="R3" library="SparkFun-Resistors" library_urn="urn:adsk.eagle:library:532" deviceset="RESISTOR" device="1206" package3d_urn="urn:adsk.eagle:package:39654/1" value="39R"/>
<part name="C1" library="Wurth_Capacitors_WCAP-CSGP" library_urn="urn:adsk.eagle:library:18488985" deviceset="WCAP-CSGP_1210_H0.95" device="_50V(DC)" package3d_urn="urn:adsk.eagle:package:18489123/2" technology="_885012009023" value="10nF"/>
<part name="R4" library="SparkFun-Resistors" library_urn="urn:adsk.eagle:library:532" deviceset="RESISTOR" device="1206" package3d_urn="urn:adsk.eagle:package:39654/1" value="470R"/>
<part name="SUPPLY1" library="Power_Symbols" library_urn="urn:adsk.eagle:library:16502351" deviceset="DGND" device="" value="DGND"/>
<part name="U3" library="SMD-PHOTOCOUPLER-5KV-LTV-816S_4P-4.6X6.5MM_" deviceset="SMD-PHOTOCOUPLER-5KV-LTV-816S(4P-4.6X6.5MM)" device=""/>
<part name="R5" library="SparkFun-Resistors" library_urn="urn:adsk.eagle:library:532" deviceset="RESISTOR" device="1206" package3d_urn="urn:adsk.eagle:package:39654/1" value="56k"/>
<part name="R6" library="SparkFun-Resistors" library_urn="urn:adsk.eagle:library:532" deviceset="RESISTOR" device="1206" package3d_urn="urn:adsk.eagle:package:39654/1" value="56k"/>
<part name="CON2" library="con-wago-500" library_urn="urn:adsk.eagle:library:195" deviceset="W237-102" device="" package3d_urn="urn:adsk.eagle:package:10688/1" value="~240v"/>
<part name="R7" library="SparkFun-Resistors" library_urn="urn:adsk.eagle:library:532" deviceset="RESISTOR" device="1206" package3d_urn="urn:adsk.eagle:package:39654/1" value="0R"/>
<part name="R8" library="SparkFun-Resistors" library_urn="urn:adsk.eagle:library:532" deviceset="RESISTOR" device="1206" package3d_urn="urn:adsk.eagle:package:39654/1" value="10k"/>
<part name="SUPPLY2" library="Power_Symbols" library_urn="urn:adsk.eagle:library:16502351" deviceset="DGND" device="" value="DGND"/>
<part name="H1" library="SparkFun-Hardware" library_urn="urn:adsk.eagle:library:519" deviceset="STAND-OFF" device="" package3d_urn="urn:adsk.eagle:package:38630/1"/>
<part name="H2" library="SparkFun-Hardware" library_urn="urn:adsk.eagle:library:519" deviceset="STAND-OFF" device="" package3d_urn="urn:adsk.eagle:package:38630/1"/>
<part name="J1" library="SparkFun-Connectors" library_urn="urn:adsk.eagle:library:513" deviceset="RASPBERRYPI-40-PIN-GPIO" device="_PTH_NO_SHROUD" package3d_urn="urn:adsk.eagle:package:38282/1"/>
<part name="R9" library="SparkFun-Resistors" library_urn="urn:adsk.eagle:library:532" deviceset="RESISTOR" device="1206" package3d_urn="urn:adsk.eagle:package:39654/1" value="0R"/>
<part name="R10" library="SparkFun-Resistors" library_urn="urn:adsk.eagle:library:532" deviceset="RESISTOR" device="1206" package3d_urn="urn:adsk.eagle:package:39654/1" value="0R"/>
<part name="SUPPLY3" library="Power_Symbols" library_urn="urn:adsk.eagle:library:16502351" deviceset="5V0-CIRCLE" device="" value="5V0"/>
<part name="SUPPLY5" library="Power_Symbols" library_urn="urn:adsk.eagle:library:16502351" deviceset="3V3-CIRCLE" device="" value="3V3"/>
<part name="H3" library="SparkFun-Hardware" library_urn="urn:adsk.eagle:library:519" deviceset="STAND-OFF" device="" package3d_urn="urn:adsk.eagle:package:38630/1"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="U1" gate="G$1" x="48.26" y="58.42" smashed="yes" rot="R90">
<attribute name="NAME" x="49.53" y="66.04" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="49.53" y="63.5" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="CON1" gate="-1" x="30.48" y="86.36" smashed="yes" rot="R270">
<attribute name="NAME" x="31.369" y="86.36" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="CON1" gate="-2" x="58.42" y="86.36" smashed="yes" rot="R270">
<attribute name="VALUE" x="40.64" y="85.217" size="1.778" layer="96"/>
<attribute name="NAME" x="59.309" y="86.36" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="U2" gate="G$1" x="76.2" y="48.26" smashed="yes" rot="MR0">
<attribute name="VALUE" x="83.841609375" y="40.618390625" size="1.783040625" layer="96" rot="MR0"/>
<attribute name="NAME" x="83.8432" y="54.11978125" size="1.783409375" layer="95" rot="MR0"/>
</instance>
<instance part="R1" gate="G$1" x="30.48" y="50.8" smashed="yes" rot="R90">
<attribute name="NAME" x="28.956" y="50.8" size="1.778" layer="95" font="vector" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="32.004" y="50.8" size="1.778" layer="96" font="vector" rot="R90" align="top-center"/>
</instance>
<instance part="R2" gate="G$1" x="58.42" y="58.42" smashed="yes" rot="R90">
<attribute name="NAME" x="56.896" y="58.42" size="1.778" layer="95" font="vector" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="59.944" y="58.42" size="1.778" layer="96" font="vector" rot="R90" align="top-center"/>
</instance>
<instance part="R3" gate="G$1" x="50.8" y="76.2" smashed="yes">
<attribute name="NAME" x="50.8" y="77.724" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="50.8" y="74.676" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="C1" gate="G$1" x="38.1" y="76.2" smashed="yes">
<attribute name="NAME" x="39.275" y="79.58" size="1.4224" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="39.015" y="71.125" size="1.4224" layer="96" align="bottom-center"/>
</instance>
<instance part="R4" gate="G$1" x="96.52" y="50.8" smashed="yes">
<attribute name="NAME" x="96.52" y="52.324" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="96.52" y="49.276" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="SUPPLY1" gate="G$1" x="91.44" y="40.64" smashed="yes">
<attribute name="VALUE" x="91.313" y="36.957" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="U3" gate="G$1" x="2.54" y="58.42" smashed="yes" rot="R180">
<attribute name="NAME" x="12.7182" y="60.96455" size="1.27228125" layer="95" rot="R270"/>
<attribute name="VALUE" x="8.90195" y="58.42" size="1.272390625" layer="96" rot="R180"/>
</instance>
<instance part="R5" gate="G$1" x="7.62" y="78.74" smashed="yes" rot="R90">
<attribute name="NAME" x="6.096" y="78.74" size="1.778" layer="95" font="vector" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="9.144" y="78.74" size="1.778" layer="96" font="vector" rot="R90" align="top-center"/>
</instance>
<instance part="R6" gate="G$1" x="-2.54" y="78.74" smashed="yes" rot="R90">
<attribute name="NAME" x="-4.064" y="78.74" size="1.778" layer="95" font="vector" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="-1.016" y="78.74" size="1.778" layer="96" font="vector" rot="R90" align="top-center"/>
</instance>
<instance part="CON2" gate="-1" x="-2.54" y="93.98" smashed="yes" rot="R270">
<attribute name="NAME" x="-1.651" y="93.98" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="CON2" gate="-2" x="7.62" y="93.98" smashed="yes" rot="R270">
<attribute name="VALUE" x="0" y="90.297" size="1.778" layer="96"/>
<attribute name="NAME" x="8.509" y="93.98" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="R7" gate="G$1" x="-2.54" y="38.1" smashed="yes" rot="R270">
<attribute name="NAME" x="-1.016" y="38.1" size="1.778" layer="95" font="vector" rot="R270" align="bottom-center"/>
<attribute name="VALUE" x="-4.064" y="38.1" size="1.778" layer="96" font="vector" rot="R270" align="top-center"/>
</instance>
<instance part="R8" gate="G$1" x="-2.54" y="22.86" smashed="yes" rot="R270">
<attribute name="NAME" x="-1.016" y="22.86" size="1.778" layer="95" font="vector" rot="R270" align="bottom-center"/>
<attribute name="VALUE" x="-4.064" y="22.86" size="1.778" layer="96" font="vector" rot="R270" align="top-center"/>
</instance>
<instance part="SUPPLY2" gate="G$1" x="96.52" y="10.16" smashed="yes">
<attribute name="VALUE" x="96.393" y="6.477" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="H1" gate="G$1" x="71.12" y="68.58" smashed="yes"/>
<instance part="H2" gate="G$1" x="71.12" y="63.5" smashed="yes"/>
<instance part="J1" gate="G$1" x="147.32" y="48.26" smashed="yes">
<attribute name="NAME" x="134.62" y="76.962" size="1.778" layer="95"/>
<attribute name="VALUE" x="134.62" y="12.7" size="1.778" layer="96"/>
</instance>
<instance part="R9" gate="G$1" x="15.24" y="33.02" smashed="yes">
<attribute name="NAME" x="15.24" y="34.544" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="15.24" y="31.496" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="R10" gate="G$1" x="15.24" y="25.4" smashed="yes">
<attribute name="NAME" x="15.24" y="26.924" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="15.24" y="23.876" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="SUPPLY3" gate="G$1" x="30.48" y="38.1" smashed="yes">
<attribute name="VALUE" x="30.353" y="41.275" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="SUPPLY5" gate="G$1" x="40.64" y="38.1" smashed="yes">
<attribute name="VALUE" x="40.513" y="41.275" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="H3" gate="G$1" x="71.12" y="73.66" smashed="yes"/>
</instances>
<busses>
<bus name="PI[1..40]">
<segment>
<wire x1="111.76" y1="50.8" x2="111.76" y2="38.1" width="0.762" layer="92"/>
<label x="114.3" y="40.64" size="1.778" layer="95"/>
<wire x1="111.76" y1="38.1" x2="127" y2="38.1" width="0.762" layer="92"/>
<wire x1="127" y1="38.1" x2="127" y2="73.66" width="0.762" layer="92"/>
</segment>
</bus>
</busses>
<nets>
<net name="N$1" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="A2"/>
<wire x1="43.18" y1="58.42" x2="30.48" y2="58.42" width="0.1524" layer="91"/>
<wire x1="30.48" y1="58.42" x2="30.48" y2="76.2" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="30.48" y1="55.88" x2="30.48" y2="58.42" width="0.1524" layer="91"/>
<junction x="30.48" y="58.42"/>
<wire x1="30.48" y1="76.2" x2="35.56" y2="76.2" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="1"/>
<pinref part="CON1" gate="-1" pin="KL"/>
<wire x1="30.48" y1="81.28" x2="30.48" y2="76.2" width="0.1524" layer="91"/>
<junction x="30.48" y="76.2"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="G"/>
<wire x1="48.26" y1="53.34" x2="48.26" y2="50.8" width="0.1524" layer="91"/>
<wire x1="48.26" y1="50.8" x2="58.42" y2="50.8" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="58.42" y1="53.34" x2="58.42" y2="50.8" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="T1"/>
<wire x1="63.5" y1="50.8" x2="58.42" y2="50.8" width="0.1524" layer="91"/>
<junction x="58.42" y="50.8"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<wire x1="58.42" y1="63.5" x2="58.42" y2="66.04" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="2"/>
<pinref part="U1" gate="G$1" pin="A1"/>
<wire x1="58.42" y1="66.04" x2="58.42" y2="76.2" width="0.1524" layer="91"/>
<wire x1="50.8" y1="58.42" x2="53.34" y2="58.42" width="0.1524" layer="91"/>
<wire x1="53.34" y1="58.42" x2="53.34" y2="66.04" width="0.1524" layer="91"/>
<wire x1="53.34" y1="66.04" x2="58.42" y2="66.04" width="0.1524" layer="91"/>
<junction x="58.42" y="66.04"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="55.88" y1="76.2" x2="58.42" y2="76.2" width="0.1524" layer="91"/>
<pinref part="CON1" gate="-2" pin="KL"/>
<wire x1="58.42" y1="81.28" x2="58.42" y2="76.2" width="0.1524" layer="91"/>
<junction x="58.42" y="76.2"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="43.18" y1="76.2" x2="45.72" y2="76.2" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="T2"/>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="63.5" y1="45.72" x2="30.48" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="1"/>
<pinref part="U2" gate="G$1" pin="A"/>
<wire x1="91.44" y1="50.8" x2="88.9" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DGND" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="K"/>
<wire x1="88.9" y1="45.72" x2="91.44" y2="45.72" width="0.1524" layer="91"/>
<pinref part="SUPPLY1" gate="G$1" pin="DGND"/>
<wire x1="91.44" y1="43.18" x2="91.44" y2="45.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="GND@6"/>
<wire x1="132.08" y1="35.56" x2="129.54" y2="35.56" width="0.1524" layer="91"/>
<wire x1="129.54" y1="35.56" x2="129.54" y2="33.02" width="0.1524" layer="91"/>
<wire x1="129.54" y1="33.02" x2="129.54" y2="30.48" width="0.1524" layer="91"/>
<wire x1="129.54" y1="30.48" x2="129.54" y2="27.94" width="0.1524" layer="91"/>
<wire x1="129.54" y1="27.94" x2="129.54" y2="25.4" width="0.1524" layer="91"/>
<wire x1="129.54" y1="25.4" x2="129.54" y2="22.86" width="0.1524" layer="91"/>
<wire x1="129.54" y1="22.86" x2="129.54" y2="20.32" width="0.1524" layer="91"/>
<wire x1="129.54" y1="20.32" x2="129.54" y2="17.78" width="0.1524" layer="91"/>
<wire x1="129.54" y1="17.78" x2="129.54" y2="15.24" width="0.1524" layer="91"/>
<wire x1="129.54" y1="15.24" x2="96.52" y2="15.24" width="0.1524" layer="91"/>
<wire x1="96.52" y1="15.24" x2="-2.54" y2="15.24" width="0.1524" layer="91"/>
<pinref part="SUPPLY2" gate="G$1" pin="DGND"/>
<wire x1="96.52" y1="12.7" x2="96.52" y2="15.24" width="0.1524" layer="91"/>
<junction x="96.52" y="15.24"/>
<pinref part="J1" gate="G$1" pin="GND@9"/>
<wire x1="132.08" y1="33.02" x2="129.54" y2="33.02" width="0.1524" layer="91"/>
<junction x="129.54" y="33.02"/>
<pinref part="J1" gate="G$1" pin="GND@14"/>
<wire x1="132.08" y1="30.48" x2="129.54" y2="30.48" width="0.1524" layer="91"/>
<junction x="129.54" y="30.48"/>
<pinref part="J1" gate="G$1" pin="GND@20"/>
<wire x1="132.08" y1="27.94" x2="129.54" y2="27.94" width="0.1524" layer="91"/>
<junction x="129.54" y="27.94"/>
<pinref part="J1" gate="G$1" pin="GND@25"/>
<wire x1="132.08" y1="25.4" x2="129.54" y2="25.4" width="0.1524" layer="91"/>
<junction x="129.54" y="25.4"/>
<pinref part="J1" gate="G$1" pin="GND@30"/>
<wire x1="132.08" y1="22.86" x2="129.54" y2="22.86" width="0.1524" layer="91"/>
<junction x="129.54" y="22.86"/>
<pinref part="J1" gate="G$1" pin="GND@34"/>
<wire x1="132.08" y1="20.32" x2="129.54" y2="20.32" width="0.1524" layer="91"/>
<junction x="129.54" y="20.32"/>
<pinref part="J1" gate="G$1" pin="GND@39"/>
<wire x1="132.08" y1="17.78" x2="129.54" y2="17.78" width="0.1524" layer="91"/>
<junction x="129.54" y="17.78"/>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="-2.54" y1="17.78" x2="-2.54" y2="15.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="R6" gate="G$1" pin="1"/>
<pinref part="U3" gate="G$1" pin="CATHODE"/>
<wire x1="-2.54" y1="73.66" x2="-2.54" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="R5" gate="G$1" pin="1"/>
<pinref part="U3" gate="G$1" pin="ANODE"/>
<wire x1="7.62" y1="73.66" x2="7.62" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="COLLECTOR"/>
<wire x1="7.62" y1="48.26" x2="7.62" y2="33.02" width="0.1524" layer="91"/>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="7.62" y1="33.02" x2="7.62" y2="25.4" width="0.1524" layer="91"/>
<wire x1="7.62" y1="25.4" x2="10.16" y2="25.4" width="0.1524" layer="91"/>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="10.16" y1="33.02" x2="7.62" y2="33.02" width="0.1524" layer="91"/>
<junction x="7.62" y="33.02"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="CON2" gate="-1" pin="KL"/>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="-2.54" y1="88.9" x2="-2.54" y2="83.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="CON2" gate="-2" pin="KL"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="7.62" y1="88.9" x2="7.62" y2="83.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PI29" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="GP5"/>
<wire x1="132.08" y1="60.96" x2="127" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="124.46" y1="20.32" x2="2.54" y2="20.32" width="0.1524" layer="91"/>
<wire x1="2.54" y1="20.32" x2="2.54" y2="30.48" width="0.1524" layer="91"/>
<pinref part="R8" gate="G$1" pin="1"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="-2.54" y1="33.02" x2="-2.54" y2="30.48" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="30.48" x2="-2.54" y2="27.94" width="0.1524" layer="91"/>
<wire x1="2.54" y1="30.48" x2="-2.54" y2="30.48" width="0.1524" layer="91"/>
<junction x="-2.54" y="30.48"/>
<wire x1="124.46" y1="38.1" x2="124.46" y2="20.32" width="0.1524" layer="91"/>
<label x="121.92" y="17.78" size="1.778" layer="95"/>
</segment>
</net>
<net name="PI31" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="GP6"/>
<wire x1="132.08" y1="58.42" x2="127" y2="58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="111.76" y1="50.8" x2="101.6" y2="50.8" width="0.1524" layer="91"/>
<label x="104.14" y="48.26" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="1"/>
<pinref part="U3" gate="G$1" pin="EMITER"/>
<wire x1="-2.54" y1="43.18" x2="-2.54" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PI2" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="5V@2"/>
<wire x1="132.08" y1="73.66" x2="129.54" y2="73.66" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="5V@4"/>
<wire x1="129.54" y1="73.66" x2="127" y2="73.66" width="0.1524" layer="91"/>
<wire x1="132.08" y1="71.12" x2="129.54" y2="71.12" width="0.1524" layer="91"/>
<wire x1="129.54" y1="71.12" x2="129.54" y2="73.66" width="0.1524" layer="91"/>
<junction x="129.54" y="73.66"/>
</segment>
<segment>
<wire x1="114.3" y1="38.1" x2="114.3" y2="33.02" width="0.1524" layer="91"/>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="114.3" y1="33.02" x2="30.48" y2="33.02" width="0.1524" layer="91"/>
<wire x1="30.48" y1="33.02" x2="20.32" y2="33.02" width="0.1524" layer="91"/>
<junction x="30.48" y="33.02"/>
<wire x1="30.48" y1="33.02" x2="30.48" y2="35.56" width="0.1524" layer="91"/>
<pinref part="SUPPLY3" gate="G$1" pin="5V0"/>
<label x="111.76" y="30.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="PI1" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="3.3V@1"/>
<wire x1="132.08" y1="68.58" x2="129.54" y2="68.58" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="3.3V@17"/>
<wire x1="129.54" y1="68.58" x2="127" y2="68.58" width="0.1524" layer="91"/>
<wire x1="132.08" y1="66.04" x2="129.54" y2="66.04" width="0.1524" layer="91"/>
<wire x1="129.54" y1="66.04" x2="129.54" y2="68.58" width="0.1524" layer="91"/>
<junction x="129.54" y="68.58"/>
</segment>
<segment>
<wire x1="119.38" y1="38.1" x2="119.38" y2="25.4" width="0.1524" layer="91"/>
<pinref part="R10" gate="G$1" pin="2"/>
<wire x1="119.38" y1="25.4" x2="40.64" y2="25.4" width="0.1524" layer="91"/>
<pinref part="SUPPLY5" gate="G$1" pin="3V3"/>
<wire x1="40.64" y1="25.4" x2="20.32" y2="25.4" width="0.1524" layer="91"/>
<wire x1="40.64" y1="35.56" x2="40.64" y2="25.4" width="0.1524" layer="91"/>
<junction x="40.64" y="25.4"/>
<label x="116.84" y="22.86" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
