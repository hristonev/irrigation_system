from queue import Queue
import logging
from collections.abc import Iterable
from typing import Dict

from src.decorators import singleton
from src.messenger.mqtt import Mqtt


@singleton
class MessengerBroker(object):
    def __init__(self, config):
        super().__init__()
        self.config = config
        self.logger = logging.getLogger()
        self.terminate_queue = []
        self.workers = []
        self.queues: Dict[str, Queue] = {}

    def get_messenger(self, config: dict, q: Queue):
        self.terminate_queue.append(Queue())
        return {
            'mqtt': lambda: Mqtt(config, self.terminate_queue[-1], q)
        }.get(config['type'])

    def init(self):
        if isinstance(self.config, Iterable):
            for m in self.config:
                self.queues[m['type']] = Queue()
                self.workers.append(self.get_messenger(m, self.queues[m['type']])())
        try:
            [w.start() for w in self.workers]
        except Exception as e:
            self.logger.critical(f'Can`t start messenger: {str(e)}')
            pass

    def send(self, messenger_type: str, message):
        self.queues[messenger_type].put(message)

    def terminate(self):
        [q.put(True) for q in self.terminate_queue]
        [w.join() for w in self.workers]
