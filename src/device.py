from __future__ import annotations

from threading import Thread
import RPi.GPIO as GPIO
import logging
from queue import Queue, Empty
from src.messenger_broker import MessengerBroker
from src.broker_message import BrokerMessage


class DeviceControlMessage:
    def __init__(self, on: bool):
        super().__init__()
        self.on = on

    def get_state(self):
        if self.on:
            return GPIO.HIGH
        else:
            return GPIO.LOW


class DeviceStatusMessage:
    def __init__(self, status: bool):
        super().__init__()
        self.status = status

    def is_working(self):
        return self.status


class Device(Thread):
    def __init__(self, pin: int, status_pin: int, messenger_type: str, control: Queue, status: Queue,
                 messenger: MessengerBroker, term: Queue, clean=False):
        super().__init__()
        self.pin = pin
        self.feedback_pin = status_pin
        self.control = control
        self.status = status
        self.term = term
        self.clean = clean
        self.messenger = messenger
        self.messenger_type = messenger_type

        self.logger = logging.getLogger()
        GPIO.setmode(GPIO.BOARD)
        GPIO.setwarnings(False)
        GPIO.setup(self.pin, GPIO.OUT)
        if self.feedback_pin > 0:
            GPIO.setup(self.feedback_pin, GPIO.IN)

    def read_status(self):
        if self.feedback_pin > 0:
            pin_status = GPIO.input(self.feedback_pin)
            self.status.put(DeviceStatusMessage(bool(pin_status)))
            self.messenger.send(self.messenger_type, BrokerMessage(f'device[{self.pin}]', pin_status))

    def run(self):
        # Init pump set stop signal as default
        GPIO.output(self.pin, GPIO.LOW)
        terminate = False
        while not terminate:
            # Send pump status
            self.read_status()
            try:
                resource: DeviceControlMessage = self.control.get(timeout=1)
                GPIO.output(self.pin, resource.get_state())
                self.logger.info(f'Device on pin[{self.pin}] set to {resource.get_state()}.')
                if self.clean:
                    GPIO.cleanup()
            except Empty:
                pass
            try:
                terminate = self.term.get(timeout=1)
            except Empty:
                pass
        GPIO.output(self.pin, GPIO.LOW)
        self.logger.info('Terminated')
