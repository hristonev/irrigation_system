from threading import Thread
from queue import Queue, Empty
from src.messenger_broker import MessengerBroker
from src.broker_message import BrokerMessage
import logging
import datetime


class Sensor(Thread):
    def __init__(self, *args, **kwargs):
        super().__init__()
        config, term, messenger = args
        self.messenger_type = config['messenger']
        self.period_sample = int(config['period_sample'])
        self.term: Queue = term
        self.messenger: MessengerBroker = messenger
        self.logger = logging.getLogger()
        self.sample_ts = datetime.datetime(1, 1, 1, 0, 0, 0)

    def check(self):
        now = datetime.datetime.now()
        delta = now - self.sample_ts
        if delta.total_seconds() > self.period_sample:
            self.sample_ts = now
            return True
        return False

    def take_sample(self):
        raise Exception("NotImplementedException")

    def send(self, topic, data):
        self.messenger.send(self.messenger_type, BrokerMessage(topic, data))

    def run(self) -> None:
        terminate = False
        while not terminate:
            if self.check():
                self.take_sample()
            try:
                terminate = self.term.get(timeout=1)
            except Empty:
                pass
        self.logger.info('Terminated')
