from smbus import SMBus
from bmp280 import BMP280
import logging
from src.sensors.sensor import Sensor


class Bmp280Sensor(Sensor):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.logger = logging.getLogger()
        self.sensor = None
        try:
            bus = SMBus(1)
            self.sensor = BMP280(i2c_dev=bus)
        except RuntimeError as e:
            self.logger.critical(str(e))

    def take_sample(self):
        try:
            self.send('temperature', self.sensor.get_temperature())
            self.send('pressure', self.sensor.get_pressure())
        except RuntimeError as e:
            self.logger.critical(str(e))
        except ZeroDivisionError:
            pass
