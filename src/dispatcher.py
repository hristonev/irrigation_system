from threading import Thread
import logging
from queue import Queue, Empty
from .device import Device
from .time_event import TimeEvent
from collections.abc import Iterable
from src.messenger_broker import MessengerBroker
from src.sensors.bmp280_sensor import Bmp280Sensor


class Dispatcher(Thread):
    def __init__(self, config, term: Queue):
        super().__init__()
        self.config = config
        self.logger = logging.getLogger()
        self.term = term
        self.sensors = {
            'bme280': lambda *args, **kwargs: Bmp280Sensor(*args, **kwargs)
        }

    def run(self):
        self.logger.info('Daemon started')
        workers = []
        terminate_queue = []
        messenger: MessengerBroker = MessengerBroker(self.config['messengers'])
        messenger.init()

        if isinstance(self.config['sensors'], Iterable):
            for s in self.config['sensors']:
                term = Queue()
                terminate_queue.append(term)
                t = s['type']
                sensor = self.sensors[t]
                workers.append(sensor(s, term, messenger))

        if isinstance(self.config['time_event'], Iterable):
            for time_data in self.config['time_event']:
                control_queue = Queue()
                status_queue = Queue()
                term = Queue()
                terminate_queue.append(term)
                workers.append(
                    TimeEvent(
                        tuple(map(int, time_data['start_time'].split(':'))),
                        time_data['duration'],
                        control_queue,
                        status_queue,
                        term,
                        self.config['general']['timezone']
                    )
                )
                term = Queue()
                terminate_queue.append(term)
                workers.append(
                    Device(
                        time_data['GPIO']['pin'],
                        time_data['GPIO']['feedback_pin'],
                        time_data['messenger'],
                        control_queue,
                        status_queue,
                        messenger,
                        term
                    )
                )
        try:
            for w in workers:
                w.start()
        except Exception as e:
            self.logger.critical(f'Threads start exception: {str(e)}')
            pass

        term = False
        while not term:
            try:
                term = self.term.get(timeout=1)
            except Empty:
                pass

        messenger.terminate()
        for q in terminate_queue:
            q.put(True)
        for w in workers:
            w.join()
