from threading import Thread
from nptime import nptime
from datetime import datetime, date, timedelta
import pytz
from queue import Queue, Empty
import logging
from src.event_interface import EventInterface
from src.device import DeviceStatusMessage, DeviceControlMessage


class TimeEvent(EventInterface, Thread):
    def __init__(self, start: tuple, duration, control: Queue, status: Queue, term: Queue, timezone='Europe/Sofia'):
        super().__init__()
        self.start_time = start
        self.duration = duration
        self.control = control
        self.status = status
        self.term = term
        self.timezone = timezone
        self.logger = logging.getLogger()

    def is_satisfy(self):
        h, m = self.start_time
        tz = pytz.timezone(self.timezone)
        start = tz.localize(
            datetime.combine(date.today(), nptime(h, m))
        )
        end = tz.localize(
            datetime.combine(date.today(), (nptime(h, m) + timedelta(seconds=self.duration)))
        )
        now = datetime.now(tz)
        return start < now < end

    def run(self):
        terminate = False
        while not terminate:
            try:
                status: DeviceStatusMessage = self.status.get(timeout=1)
                if not status.is_working() and self.is_satisfy():
                    self.control.put(
                        DeviceControlMessage(True)
                    )
                elif status.is_working() and not self.is_satisfy():
                    self.control.put(
                        DeviceControlMessage(False)
                    )
            except Empty:
                pass
            try:
                terminate = self.term.get(timeout=1)
            except Empty:
                pass
        self.logger.info('Terminated')
