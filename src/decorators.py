import functools


def singleton(class_name):
    prev_instance = dict()

    @functools.wraps(class_name)
    def wrapper(*args, **kwargs):
        if class_name in prev_instance and prev_instance.get(class_name, None).get('args') == (args, kwargs):
            return prev_instance.get(class_name).get('instance')
        else:
            prev_instance[class_name] = {
                'args': (args, kwargs),
                'instance': class_name(*args, **kwargs)
            }
            return prev_instance[class_name].get('instance')
    return wrapper
