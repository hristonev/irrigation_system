from threading import Thread
from queue import Queue, Empty
import logging
import paho.mqtt.client as mqtt
import time
from src.broker_message import BrokerMessage


class Mqtt(Thread):
    def __init__(self, config: dict, term: Queue, queue: Queue):
        super().__init__()
        self.config = config
        self.term = term
        self.queue = queue
        self.logger = logging.getLogger()
        self.client = mqtt.Client()
        # self.client.on_connect =self.on_connect
        # self.client.on_message = self.on_message
        self.is_connected = False

    def on_connect(self, client, userdata, flags, rc):
        print(client)
        print(userdata)
        print(flags)
        self.is_connected = True
        self.logger.info(f'Connected result code: {str(rc)}')

    def on_message(self, client, userdata, msg):
        self.logger.info(f'{msg.topic} new message: {str(msg.payload)}')

    def connect(self):
        try:
            self.client.connect(self.config['host'], self.config['port'], self.config.get('timeout', 60))
            self.is_connected = True
        except ConnectionRefusedError as e:
            self.logger.critical(str(e))

    def is_terminated(self):
        try:
            return bool(self.term.get(timeout=1))
        except Empty:
            return False

    def run(self) -> None:
        terminate = False
        self.connect()
        while not terminate and not self.is_connected:
            time.sleep(5)
            try:
                self.logger.info('Reconnecting...')
                self.client.reconnect()
                self.is_connected = True
            except ConnectionRefusedError as e:
                self.logger.critical(str(e))
            terminate = self.is_terminated()
        if self.is_connected:
            self.client.loop_start()
        topic = self.config['topic']
        while not terminate and self.is_connected:
            try:
                message: BrokerMessage = self.queue.get(timeout=1)
                self.client.publish(f'{topic}/{message.topic}', message.message)
            except Empty:
                pass
            terminate = self.is_terminated()
        if self.is_connected:
            self.client.disconnect()
        self.logger.info('Terminated')

